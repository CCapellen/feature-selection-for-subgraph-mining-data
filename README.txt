Dieses Repository enth�lt im Ordner 'Programm' den Code, der f�r die Experimente 
im Rahmen der Bachelorarbeit "Feature Selection f�r Subgraph Mining Daten" 
erstellt wurde. Er ist nur lauff�hig, wenn die ben�tigten Datens�tze zur Verf�gung stehen.

Die urspr�nglichen Daten m�ssen daher in Programm/ba/ eingef�gt werden und anschlie�end 
muss preprocessData.py ausgef�hrt werden.

Das Hauptmodul ist "experiment.py". Dieses ruft alle anderen Module auf, 
die f�r eine vollst�ndige Kreuzvalidierung mit ausgew�hlten Algorithmen ben�tigt werden. 
Hier k�nnen auch alle Voreinstellungen (getestete Algorithmen, Parameter...) vorgenommen werden.

Ein Interface zu den meisten verwendeten Algorithmen findet sich in featureSelection.py

Auf verschiedenen Computern (selbst mit gleichen Python, Anaconda und Modulversionen) kommt
es gelegentlich zu Problemen mit dem Einbinden von SelectFromModel und MRMR in featureSelection.py
indem Fall m�ssen der Import der Module dort auskommentiert werden. Die restlichen Algorithmen 
k�nnen dann verwendet werden.