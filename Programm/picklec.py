# -*- coding: utf-8 -*-
"""
@author: Maren, Cat

Kleines tool das pickle Dateien, die mit Python 3 gepeichert wurden in
Pickle von Python 2 konvertiert
(In Python 3 Umgebung ausführen)
"""


import pickle

foldername = 'Experiment2Cat/Ergebnisse2Cat/'
tofoldername = 'Experiment2Cat/Ergebnisse2Cat/'

picklename = 'AIDS99_connected.k1.t800features.binarizelogisticRegressionscoringVLINEARrv.p'
rv = pickle.load( open( "sets/" + foldername + picklename, "rb" ) )
pickle.dump(rv, open( "sets/" + tofoldername + picklename, "wb" ), protocol=2)

