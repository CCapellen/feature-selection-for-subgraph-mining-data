# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 09:19:48 2016

@author: Maren, Catherine

Hier werden die im Experiment ausgewählten FS-Algorithmen getestet.
Dazu wird zunächst die Performanz ohne FS ermittelt und anschließend eine
Feature Selection durchgeführt und die Klassifikatoren erneut gestestet.
Alle dabei erhobenen Daten werden in einem Dictionary 'rv' gespeichert.
"""

import numpy as np
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
import scipy
import scipy.sparse as sp
import featureSelection
import performance
from sklearn.tree import DecisionTreeClassifier
import time
import tuning
import classifierTuning
import sets2
from sklearn.metrics import roc_auc_score
#import pickle

""" Funktion zum Laden von sparse, csr Matrizen """ 
def load_sparse_csr(filename):
    loader = np.load(filename)
    return sp.csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])


def training(algos, fsParas, clParas, dataname, foldername, oneOut=True, trainRuns=5, tuneruns=5): # Parameter der Klass.-Algos sollen hier übergeben werden # dataname

    # return values
    rv = {"setname":dataname,
         "noFSResults":np.zeros((3,6),dtype='float64'),
         "noFSResultsRoc":np.zeros((2),dtype='float64'),
         "classifyNoFStime":np.zeros((6,),dtype='float64'),
         "scoringVResults":np.zeros((3,6),dtype='float64'),
         "scoringVResultsRoc":np.zeros((2),dtype='float64'),
         "sVclassifyFStime":np.zeros((6,),dtype='float64'),
         "scoringCResults":np.zeros((3,6),dtype='float64'),
         "scoringCResultsRoc":np.zeros((2),dtype='float64'),
         "sCclassifyFStime":np.zeros((6,),dtype='float64'), 
         "scoringFResults":np.zeros((3,6),dtype='float64'),
         "scoringFResultsRoc":np.zeros((2),dtype='float64'), 
         "sFVclassifyFStime":np.zeros((6,),dtype='float64'), 
         "dTResults":np.zeros((3,6),dtype='float64'),
         "dTResultsRoc":np.zeros((2),dtype='float64'), 
         "dTclassifyFStime":np.zeros((6,),dtype='float64'),
         "reliefResults":np.zeros((3,6),dtype='float64'),
         "reliefResultsRoc":np.zeros((2),dtype='float64'), 
         "reliefclassifyFStime":np.zeros((6,),dtype='float64'),
         "lRResults":np.zeros((3,6),dtype='float64'),
         "lRResultsRoc":np.zeros((2),dtype='float64'), 
         "lRclassifyFStime":np.zeros((6,),dtype='float64'), 
         "rFResults":np.zeros((3,6),dtype='float64'),
         "rFResultsRoc":np.zeros((2),dtype='float64'), 
         "rFclassifyFStime":np.zeros((6,),dtype='float64'),
         "rFEResults":np.zeros((3,6),dtype='float64'),
         "rFEResultsRoc":np.zeros((2),dtype='float64'), 
         "rFEclassifyFStime":np.zeros((6,),dtype='float64'),
         "dSResults":np.zeros((3,6),dtype='float64'),
         "dSResultsRoc":np.zeros((2),dtype='float64'),
         "dSclassifyFStime":np.zeros((6,),dtype='float64'),
         "gaResults":np.zeros((3,6),dtype='float64'),
         "gaResultsRoc":np.zeros((2),dtype='float64'), 
         "gaclassifyFStime":np.zeros((6,),dtype='float64'),
         "mrmrResults":np.zeros((3,6),dtype='float64'),
         "mrmrResultsRoc":np.zeros((2),dtype='float64'),
         "mrmrclassifyFStime":np.zeros((6,),dtype='float64'),
         "posResults":np.zeros((3,6),dtype='float64'),
         "posResultsRoc":np.zeros((2),dtype='float64'),
         "posclassifyFStime":np.zeros((6,),dtype='float64'),


         "optParasScoringV":[],"optParasLR":[],    
         "optParasScoringC":[], "optParasScoringF":[],
         "optParasRF":[], "optParasDT":[], "optParasRelief":[],
         "optParasRFE":[], "optParasDS":[], "optParasMrmr":[],
         "optParasGA":[], "handParasGA":[],
         "optParasPOS":[],
          
         "sVFStime":0, "nFeatScoringV":0, 
         "sCFStime":0, 
         "sFVFStime":0,
         "dTFStime":0,
         "reliefFStime":0,
         "lRFStime":0, "nFeatLR":0,
         "rFFStime":0,  
         "rFEFStime":0,
         "dSFStime":0, 
         "gaFStime":0, "nFeatGA":0,
         "mrmrFStime":0,
         "posFStime":0, "nFeatPos":0}
    
    starttime = time.time()
    
    #Öffne Daten (ursprünglich aus sets)    
    oO = "withOnes"
    if oneOut:
        oO = "oneOut"     
   
    data = load_sparse_csr("baNew/" + dataname + ".sparse." + oO + ".npz")
    data = data.tocsc()   
   
    labels = scipy.io.mmread("baNew/"+ dataname + ".label." + oO)  # die zugehörigen Graph-Label
    labels = labels.tocsc()
    
    setstime0 = time.time()-starttime
    print("Time for setup (initial loading of sets): ", setstime0)
    
    
    for i in range(trainRuns):    
        
        #Sets erstellen anhand gespeicherter Indizes
        starttime = time.time()
        
        testindis = np.load("sets/" + foldername + "testindis"+str(i+1)+"."+dataname+"."+oO+".npy")
        trainindis = np.load("sets/" + foldername + "trainindis"+str(i+1)+"."+dataname+"."+oO+".npy")       

        testdata = data[testindis]
        testlabels = labels[testindis]
        traindata = data[trainindis]
        trainlabels = labels[trainindis]
        
        testdata = testdata.tocsc()  
        traindata = traindata.tocsc()
        trainlabels = trainlabels.todense() 
        trainlabels = np.ravel(trainlabels)
        testlabels = testlabels.todense() 
        testlabels = np.ravel(testlabels)         
        
        #Kleinere Sets für Relief und Mrmr
        if algos.get('relief',0) or algos.get('mrmr',0):
            sizeSmallSet = int(traindata.shape[0]/30.)
            smallerTraindata, smallerTrainlabels, oldIndis = sets2.produceSmallerSets2(traindata, trainlabels, sizeSmallSet)       
        
        setstime = time.time()-starttime
        print("time for loading sets (in Trainrun "+str(i+1)+"): " + str(setstime) + "\n\n  Tuning starts now")
          
        if algos.get('noFS',0):
            #Lasse alle Algos ohne FS klassifizieren
            optClParas = classifierTuning.tuning(tuneruns, traindata, trainlabels, clParas)
            noFSResultsDict = classifyNoFS(testdata, testlabels, traindata, trainlabels, optClParas)
            print("Tuning beendet")
            noFSResults = np.array((noFSResultsDict['perfSVM'], noFSResultsDict['perfBayes'], noFSResultsDict['perfDT']))     
            noFSResultsRoc = np.array((noFSResultsDict['rocSVM'], noFSResultsDict['rocBayes']))
            classifyNoFStime = noFSResultsDict['times']     
            print("Ohne FS erzielen die Algorithmen eine Performance von: \n \n SVM: \n"+ str(noFSResultsDict['perfSVM'])+ "\n\n Bayes:\n "+ str(noFSResultsDict['perfBayes'])+ "\n \n Decision Tree: \n"+ str(noFSResultsDict['perfDT']))       
            print("\ntimes of Classification before featureselection: " + str(classifyNoFStime)+ "\nNach der Feature Selection:")
        rv["originalMatrixDim"] = traindata.shape

        #Aufruf von Tuning
        rvTuning = tuning.tuning(tuneruns, traindata, trainlabels, algos,fsParas, clParas)
        
        #scoring mit VarianceThreshold
        if algos.get('scoringV',0):       
            starttime = time.time()        
            scoringVIndices = featureSelection.scoringVariance(traindata, trainlabels)
            sVFStime = time.time()-starttime
            optClParas = classifierTuning.tuning(tuneruns, traindata[:,scoringVIndices], trainlabels, clParas)
            scoringVResults = classifyNoFS(testdata[:,scoringVIndices], testlabels, traindata[:,scoringVIndices], trainlabels,optClParas)
            #print("Mit Varianz-Scoring-FS erzielen die Algorithmen eine Performance von: \n \n (SVM:) \n" + str(scoringVResults[0]) + " \n \n (Bayes:) \n" + str(scoringVResults[1]) + " \n \n (Decision Tree :) \n" + str(scoringVResults[2]))  
            #print("Es wurden " + str(scoringVIndices.shape[0]) + "Features ausgewählt.")        
            #print("Laufzeit der FeatureSelection: " + str(sVFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(sVclassifyFStime))         
            
            rv["scoringVResults"][0] += (scoringVResults['perfSVM']/np.float(trainRuns))
            rv["scoringVResults"][1] += (scoringVResults['perfBayes']/np.float(trainRuns))
            rv["scoringVResults"][2] += (scoringVResults['perfDT']/np.float(trainRuns))
            rv["scoringVResultsRoc"][0] += (scoringVResults['rocSVM']/np.float(trainRuns))
            rv["scoringVResultsRoc"][1] += (scoringVResults['rocBayes']/np.float(trainRuns))
            rv["sVclassifyFStime"] += (scoringVResults['times']/np.float(trainRuns))
            rv["sVFStime"] += (sVFStime/np.float(trainRuns))
            rv["nFeatScoringV"] += (scoringVIndices.shape[0]/np.float(trainRuns))
            rv["optParasScoringV"].append(optClParas)
          
        
        #scoring mit chi2
        if algos.get('scoringC',0):
            threshSVM = rvTuning['scoringCSVM'][1][0]
            threshBayes = rvTuning['scoringCBayes'][1][0]
            threshDT = rvTuning['scoringCDT'][1][0]
            maxThreshold = np.max([threshSVM,threshBayes,threshDT])
            starttime = time.time()
            scoringCIndices = featureSelection.scoringChi2(traindata, trainlabels, maxThreshold)
            #print('scoringINdices.shape', scoringCIndices.shape)
            #print('3Mengen',[testdata[:,scoringCIndices[:threshSVM]],testdata[:,scoringCIndices[:threshBayes]],testdata[:,scoringCIndices[:threshDT]]])
            sCFStime = time.time()-starttime 
            scoringCResults = classify(np.array([testdata[:,scoringCIndices[:threshSVM]],testdata[:,scoringCIndices[:threshBayes]],testdata[:,scoringCIndices[:threshDT]]]), testlabels,np.array([traindata[:,scoringCIndices[:threshSVM]],traindata[:,scoringCIndices[:threshBayes]],traindata[:,scoringCIndices[:threshDT]]]), trainlabels,rvTuning['scoringCSVM'][0],rvTuning['scoringCDT'][0])
            #print("Mit Chi2-Scoring-FS erzielen die Algorithmen eine Performance von: \n \n (SVM:) \n"+ str(scoringCResults[0]) + " \n \n (Bayes:) \n" + str(scoringCResults[1]) + " \n \n (Decision Tree :) \n" + str(scoringCResults[2]))  
            #print("Laufzeit der FeatureSelection: " + str(sCFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(sCclassifyFStime)) 

            rv["scoringCResults"][0] += (scoringCResults['perfSVM']/np.float(trainRuns))
            rv["scoringCResults"][1] += (scoringCResults['perfBayes']/np.float(trainRuns))
            rv["scoringCResults"][2] += (scoringCResults['perfDT']/np.float(trainRuns))
            rv["scoringCResultsRoc"][0] += (scoringCResults['rocSVM']/np.float(trainRuns))
            rv["scoringCResultsRoc"][1] += (scoringCResults['rocBayes']/np.float(trainRuns))
            rv["sCclassifyFStime"] += (scoringCResults['times']/np.float(trainRuns))
            rv["sCFStime"] += (sCFStime/np.float(trainRuns))
            rv["optParasScoringC"].append([rvTuning['scoringCSVM'],rvTuning['scoringCBayes'],rvTuning['scoringCDT']])
         
        
        #scoring mit f-Value
        if algos.get('scoringF',0):
            threshSVM = rvTuning['scoringFSVM'][1][0]
            threshBayes = rvTuning['scoringFBayes'][1][0]
            threshDT = rvTuning['scoringFDT'][1][0]
            maxThreshold = np.max([threshSVM,threshBayes,threshDT])
            starttime = time.time()        
            scoringFIndices = featureSelection.scoringFValue(traindata, trainlabels, maxThreshold)
            sFVFStime = time.time()-starttime 
            scoringFResults = classify(np.array([testdata[:,scoringFIndices[:threshSVM]],testdata[:,scoringFIndices[:threshBayes]],testdata[:,scoringFIndices[:threshDT]]]), testlabels,np.array([traindata[:,scoringFIndices[:threshSVM]],traindata[:,scoringFIndices[:threshBayes]],traindata[:,scoringFIndices[:threshDT]]]), trainlabels,rvTuning['scoringFSVM'][0],rvTuning['scoringFDT'][0])
            #print("Mit F-Value-Scoring-FS erzielen die Algorithmen eine Performance von: \n \n (SVM:) \n" + str(scoringFResults[0]) + " \n \n (Bayes:) \n" + str(scoringFResults[1]) + " \n \n Decision Tree : \n" + str(scoringFResults[2]))           
            #print("Es wurden " + str(threshold) + "Features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(sFVFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(sFVclassifyFStime)) 
            
            print("scoringFResults['perfSVM']",scoringFResults['perfSVM'])
            print("scoringFResults['perfBayes']",scoringFResults['perfBayes'])
            print("scoringFResults['perfDT']",scoringFResults['perfDT'])
            
            rv["scoringFResults"][0] += (scoringFResults['perfSVM']/np.float(trainRuns))
            rv["scoringFResults"][1] += (scoringFResults['perfBayes']/np.float(trainRuns))
            rv["scoringFResults"][2] += (scoringFResults['perfDT']/np.float(trainRuns))
            rv["scoringFResultsRoc"][0] += (scoringFResults['rocSVM']/np.float(trainRuns))
            rv["scoringFResultsRoc"][1] += (scoringFResults['rocBayes']/np.float(trainRuns))            
            rv["sFVclassifyFStime"] += (scoringFResults['times']/np.float(trainRuns))
            rv["sFVFStime"] += (sFVFStime/np.float(trainRuns))
            rv["optParasScoringF"].append([rvTuning['scoringFSVM'],rvTuning['scoringFBayes'],rvTuning['scoringFDT']])
           
     
        # Decision Tree Filtering
        if algos.get('decisionTreeFilter',0):
            starttime = time.time()
            dtIndicesSVM = featureSelection.decisionTreeFilter(traindata, trainlabels, rvTuning['dtSVM'][1][0],rvTuning['dtSVM'][1][1])
            dtIndicesBayes = featureSelection.decisionTreeFilter(traindata, trainlabels, rvTuning['dtBayes'][1][0],rvTuning['dtBayes'][1][1])
            dtIndicesDT = featureSelection.decisionTreeFilter(traindata, trainlabels, rvTuning['dtDT'][1][0],rvTuning['dtDT'][1][1])
            dTFStime = (time.time()-starttime)/3. #TODO ##
            dtResults = classify(np.array([testdata[:,dtIndicesSVM],testdata[:,dtIndicesBayes],testdata[:,dtIndicesDT]]), testlabels,np.array([traindata[:,dtIndicesSVM],traindata[:,dtIndicesBayes],traindata[:,dtIndicesDT]]), trainlabels,rvTuning['dtSVM'][0],rvTuning['dtDT'][0])
            #print("Mit Decision Tree Filtering (threshold = " + str(threshold) + ") erzielen die Algorithmen eine Performance von:  \n\n SVM: \n" + str(dTResults[0]) + " \n\n Bayes: \n " + str(dTResults[1]) + " \n\n Decision Tree: \n " + str(dTResults[2]))
            #print("Es wurden " + str(decisionTreeIndizes.shape[0]) +" features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(dTFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(dTclassifyFStime)) 
            
            rv["dTResults"][0] += (dtResults['perfSVM']/np.float(trainRuns)) 
            rv["dTResults"][1] += (dtResults['perfBayes']/np.float(trainRuns))   
            rv["dTResults"][2] += (dtResults['perfDT']/np.float(trainRuns))
            rv["dTResultsRoc"][0] += (dtResults['rocSVM']/np.float(trainRuns))
            rv["dTResultsRoc"][1] += (dtResults['rocBayes']/np.float(trainRuns))
            rv["dTclassifyFStime"] += (dtResults['times']/np.float(trainRuns))
            rv["dTFStime"] += (dTFStime/np.float(trainRuns))
            rv["optParasDT"].append([rvTuning['dtSVM'],rvTuning['dtBayes'],rvTuning['dtDT']])
        
                   
        #Relief 
        if algos.get('relief',0):
            starttime = time.time()
            reliefM = 5
            nFeatRelief = 10
            reliefIndices = featureSelection.relief(smallerTraindata, smallerTrainlabels,reliefM, nFeatRelief)
            
            #Ermittlung der tatsächlichen Indizes (wegen der kleineren Mengen)
            trueIndices = []
            for indi in reliefIndices:
                trueIndices.append(oldIndis[indi])
            trueIndices = np.array(trueIndices)
            reliefSelectedTestdata = testdata[:,trueIndices]
            reliefSelectedTraindata = traindata[:,trueIndices] 
            optClParas = classifierTuning.tuning(tuneruns, reliefSelectedTraindata, trainlabels, clParas)
            reliefResults = classifyNoFS(reliefSelectedTestdata, testlabels, reliefSelectedTraindata, trainlabels, optClParas)
            print(reliefResults)
            reliefFStime = time.time()-starttime
            
            rv["reliefResults"][0] += (reliefResults['perfSVM']/np.float(trainRuns))
            rv["reliefResults"][1] += (reliefResults['perfBayes']/np.float(trainRuns))
            rv["reliefResults"][2] += (reliefResults['perfDT']/np.float(trainRuns))
            rv["reliefResultsRoc"][0] += (reliefResults['rocSVM']/np.float(trainRuns))
            rv["reliefResultsRoc"][1] += (reliefResults['rocBayes']/np.float(trainRuns))
            rv["reliefclassifyFStime"] += (reliefResults['times']/np.float(trainRuns))
            rv["reliefFStime"] += (reliefFStime/np.float(trainRuns))
            rv["optParasRelief"].append(optClParas)
            rv["nFeatRelief"] = nFeatRelief
            rv["reliefM"] = reliefM
            rv["sizeSmallSet"] = sizeSmallSet
       
        if algos.get('mrmr',0):
            starttime = time.time()
            mrmrIndices = featureSelection.mrmr(smallerTraindata, smallerTrainlabels,fsParas['mrmrNFeat'])
            
            #Ermittlung der tatsächlichen Indizes
            trueIndices = []
            for indi in mrmrIndices:
                trueIndices.append(oldIndis[indi])
            trueIndices = np.array(trueIndices)
            mrmrSelectedTestdata = testdata[:,trueIndices]
            mrmrSelectedTraindata = traindata[:,trueIndices] 
            optClParas = classifierTuning.tuning(tuneruns, mrmrSelectedTraindata, trainlabels, clParas)
            mrmrResults = classifyNoFS(mrmrSelectedTestdata, testlabels, mrmrSelectedTraindata, trainlabels, optClParas)
            print(mrmrResults)
            mrmrFStime = time.time()-starttime
            
            rv["mrmrResults"][0] += (mrmrResults['perfSVM']/np.float(trainRuns))
            rv["mrmrResults"][1] += (mrmrResults['perfBayes']/np.float(trainRuns))
            rv["mrmrResults"][2] += (mrmrResults['perfDT']/np.float(trainRuns))
            rv["mrmrResultsRoc"][0] += (mrmrResults['rocSVM']/np.float(trainRuns))
            rv["mrmrResultsRoc"][1] += (mrmrResults['rocBayes']/np.float(trainRuns))
            rv["mrmrclassifyFStime"] += (mrmrResults['times']/np.float(trainRuns))
            rv["mrmrFStime"] += (mrmrFStime/np.float(trainRuns))
            rv["optParasMrmr"].append(optClParas)
       
        #Logistic Regression
        if algos.get('logisticRegression',0):
            starttime = time.time()
            lRIndices = featureSelection.logisticRegression(traindata, trainlabels)
            lRFStime = time.time()-starttime  
            optClParas = classifierTuning.tuning(tuneruns, traindata[:,lRIndices], trainlabels, clParas)
            lRResults = classifyNoFS(testdata[:,lRIndices], testlabels, traindata[:,lRIndices], trainlabels,optClParas)
            #print("Mit Logistic Regression erzielen die Algorithmen eine Performance von: \n\nSVM: \n" + str(lRResults[0]) + " \n\n Bayes:\n " + str(lRResults[1]) +"\n \n Decision Tree: \n"  + str(lRResults[2]))      
            #print("Es wurden " + str(lRIndices.shape[0]) + " Features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(lRFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(lRclassifyFStime)) 
            
            rv["lRResults"][0] += (lRResults['perfSVM']/np.float(trainRuns))
            rv["lRResults"][1] += (lRResults['perfBayes']/np.float(trainRuns))
            rv["lRResults"][2] += (lRResults['perfDT']/np.float(trainRuns))
            rv["lRResultsRoc"][0] += (lRResults['rocSVM']/np.float(trainRuns))
            rv["lRResultsRoc"][1] += (lRResults['rocBayes']/np.float(trainRuns))
            rv["lRclassifyFStime"] += (lRResults['times']/np.float(trainRuns))
            rv["lRFStime"] += lRFStime/np.float(trainRuns)
            rv["nFeatLR"] += (lRIndices.shape[0]/np.float(trainRuns))
            rv["optParasLR"].append(optClParas)
        
                   
        # Random Forest Feature Selection 
        if algos.get('randomForestFeatureSelection',0):
            starttime = time.time()           
            rfIndicesSVM = featureSelection.randomForestFS(traindata, trainlabels,rvTuning['rfSVM'][1][0],rvTuning['rfSVM'][1][1])
            rfIndicesBayes = featureSelection.randomForestFS(traindata, trainlabels,rvTuning['rfBayes'][1][0],rvTuning['rfBayes'][1][1])
            rfIndicesDT = featureSelection.randomForestFS(traindata, trainlabels,rvTuning['rfDT'][1][0],rvTuning['rfDT'][1][1])
            rfFStime = (time.time()-starttime)/3.             
            rfResults= classify(np.array([testdata[:,rfIndicesSVM],testdata[:,rfIndicesBayes],testdata[:,rfIndicesDT]]), testlabels,np.array([traindata[:,rfIndicesSVM],traindata[:,rfIndicesBayes],traindata[:,rfIndicesDT]]), trainlabels,rvTuning['rfSVM'][0],rvTuning['rfDT'][0])
            #print("Mit Random Forest Feature Selection (numTrees = " + str(numTrees) + "; threshold = " +str(d) + ") erzielen die Algorithmen eine Performance von: \n\n SVM: \n" + str(rFResults[0]) +" \n\n Bayes:\n " + str(rFResults[1]) + "\n \n Decision Tree: \n" +  str(rFResults[2]))   
            #print("Es wurden " + str(rFIndices.shape[0]) + " Features ausgewaehlt.")      
            #print("Laufzeit der FeatureSelection: " + str(rFFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(rFclassifyFStime))        

            rv["rFResults"][0] += (rfResults['perfSVM']/np.float(trainRuns))
            rv["rFResults"][1] += (rfResults['perfBayes']/np.float(trainRuns))
            rv["rFResults"][2] += (rfResults['perfDT']/np.float(trainRuns))
            rv["rFResultsRoc"][0] += (rfResults['rocSVM']/np.float(trainRuns))
            rv["rFResultsRoc"][1] += (rfResults['rocBayes']/np.float(trainRuns))
            rv["rFclassifyFStime"] += (rfResults['times']/np.float(trainRuns))
            rv["rFFStime"] += (rfFStime/np.float(trainRuns))
            rv["optParasRF"].append([rvTuning['rfSVM'],rvTuning['rfBayes'],rvTuning['rfDT']])
       
            
        #recursive Feature Elimination
        if algos.get('recursiveFeatureElimination',0):
            steps = rvTuning['rfeSteps']
            nFeatToSelect = rvTuning['rfeNFeat']
            order = np.argsort(nFeatToSelect)[::-1] 
            rFEIndices = [0,0,0]
            starttime = time.time() 
            rFEIndices[order[0]] = featureSelection.recursiveFeatureElimination(traindata,trainlabels, steps[order[0]],nFeatToSelect[order[0]])
            rFEFStime = time.time()-starttime 
            
            # if Abfragen zur Verhinderung unnötiger, teurer Berechnung
            if steps[order[1]] == steps[order[0]]:
                rFEIndices[order[1]] =featureSelection.recursiveFeatureElimination(traindata[:,rFEIndices[order[0]]],trainlabels, steps[order[1]],nFeatToSelect[order[1]])
            else:
                rFEIndices[order[1]] =featureSelection.recursiveFeatureElimination(traindata,trainlabels, steps[order[1]],nFeatToSelect[order[1]])
            if steps[order[2]] == steps[order[0]]:
                rFEIndices[order[2]] =featureSelection.recursiveFeatureElimination(traindata[:,rFEIndices[order[0]]],trainlabels, steps[order[2]],nFeatToSelect[order[2]])
            elif steps[order[2]] == steps[order[1]]:
                rFEIndices[order[2]] =featureSelection.recursiveFeatureElimination(traindata[:,rFEIndices[order[1]]],trainlabels, steps[order[2]],nFeatToSelect[order[2]])
            else:
                rFEIndices[order[2]] =featureSelection.recursiveFeatureElimination(traindata,trainlabels, steps[order[2]],nFeatToSelect[order[2]])
            
            rFEResults = classify([testdata[:,rFEIndices[0]], testdata[:,rFEIndices[1]],testdata[:,rFEIndices[2]]],testlabels, [traindata[:,rFEIndices[0]], traindata[:,rFEIndices[1]], traindata[:,rFEIndices[2]]], trainlabels, rvTuning['rfeSVM'][0],rvTuning['rfeDT'][0])
            #print("Mit RFE-FS (step=" + str(step) + ") erzielen die Algorithmen eine Performance von: \n\n SVM: \n" + str(rFEResults[0]) + " \n\n Bayes:\n " + str(rFEResults[1]) +"\n \n Decision Tree: \n" +  str(rFEResults[2]))
            #print("Es wurden " + str(rFEIndices.shape[0]) + " Features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(rFEFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(rFEclassifyFStime))       
             
            rv["rFEResults"][0] += (rFEResults['perfSVM']/np.float(trainRuns))
            rv["rFEResults"][1] += (rFEResults['perfBayes']/np.float(trainRuns))
            rv["rFEResults"][2] += (rFEResults['perfDT']/np.float(trainRuns))
            rv["rFEResultsRoc"][0] += (rFEResults['rocSVM']/np.float(trainRuns))
            rv["rFEResultsRoc"][1] += (rFEResults['rocBayes']/np.float(trainRuns))
            rv["rFEclassifyFStime"] += (rFEResults['times']/np.float(trainRuns))
            rv["rFEFStime"] += (rFEFStime/np.float(trainRuns))
            rv["optParasRFE"].append([rvTuning['rfeSVM'],rvTuning['rfeBayes'],rvTuning['rfeDT']])
        
        # genetischer Wrapper
        if algos.get('geneticAlgorithm',0):
            nGenes = 50
            nIt = 50
            mutRate = 0.1
            classifier = 'DT'
            #nFeaturesToConsider = 10
            starttime = time.time()    
            svmRFIndizes = featureSelection.geneticAlgorithm(traindata,trainlabels,nIt,nGenes,mutRate,classifier)
            svmRFFStime =  time.time()-starttime   
            optClParas = classifierTuning.tuning(tuneruns, traindata[:,svmRFIndizes], trainlabels, clParas)
            svmRFResults = classifyNoFS(testdata[:,svmRFIndizes],testlabels, traindata[:,svmRFIndizes], trainlabels,optClParas)
            #print("Mit svmRF (nFeaturesToSelect =" + str(nFeaturesToSelect) + "nFeaturesToConsider = " + str(nFeaturesToConsider) + ",  erzielen die Algorithmen eine Performance von: \n\n SVM: \n" + str(svmRFResults[0]) + " \n\n Bayes:\n " + str(svmRFResults[1]) +"\n \n Decision Tree: \n" +  str(svmRFResults[2]))
            #print("Es wurden " + str(svmRFIndizes.shape[0]) + " Features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(svmRFFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(svmRFclassifyFStime))               
            
            rv["gaResults"][0] += (svmRFResults['perfSVM']/np.float(trainRuns))
            rv["gaResults"][1] += (svmRFResults['perfBayes']/np.float(trainRuns))
            rv["gaResults"][2] += (svmRFResults['perfDT']/np.float(trainRuns))
            rv["gaResultsRoc"][0] += (svmRFResults['rocSVM']/np.float(trainRuns))
            rv["gaResultsRoc"][1] += (svmRFResults['rocBayes']/np.float(trainRuns))
            rv["gaclassifyFStime"] += (svmRFResults['times']/np.float(trainRuns))
            rv["gaFStime"] += (svmRFFStime/np.float(trainRuns))          
            rv["optParasGA"].append(optClParas)
            rv["handParasGA"]=[nIt,nGenes,mutRate,classifier]
            rv['nFeatGA'] += np.sum(svmRFIndizes)/np.float(trainRuns)

        # Decision Stumps
        if algos.get('decisionStumps',0):
            threshSVM = rvTuning['dsSVM'][1][0]
            threshBayes = rvTuning['dsBayes'][1][0]
            threshDT = rvTuning['dsDT'][1][0]
            maxThreshold = np.max([threshSVM,threshBayes,threshDT])
            starttime = time.time()        
            dsIndices = featureSelection.decisionStumps(traindata, trainlabels, maxThreshold)
            dsFStime = time.time()-starttime 
            dsResults = classify(np.array([testdata[:,dsIndices[:threshSVM]],testdata[:,dsIndices[:threshBayes]],testdata[:,dsIndices[:threshDT]]]), testlabels,np.array([traindata[:,dsIndices[:threshSVM]],traindata[:,dsIndices[:threshBayes]],traindata[:,dsIndices[:threshDT]]]), trainlabels,rvTuning['dsSVM'][0],rvTuning['dsDT'][0])
            
            #print("Mit Decision Stumps (d=" + str(d) + ") erzielen die Algorithmen eine Performance von: \n\n SVM: \n" + str(decisionStumpResults[0]) + " \n\n Bayes:\n " + str(decisionStumpResults[1]) +"\n \n Decision Tree: \n" +  str(decisionStumpResults[2]))
            #print("Es wurden " + str(decisionStumpIndices.shape[0]) + " Features ausgewählt.")
            #print("Laufzeit der FeatureSelection: " + str(dSFStime) + "\nLaufzeit der Klassifikation mit ausgewaehlten Features: " + str(dSclassifyFStime))       
            
            rv["dSResults"][0] += (dsResults['perfSVM']/np.float(trainRuns))
            rv["dSResults"][1] += (dsResults['perfBayes']/np.float(trainRuns))
            rv["dSResults"][2] += (dsResults['perfDT']/np.float(trainRuns))
            rv["dSResultsRoc"][0] += (dsResults['rocSVM']/np.float(trainRuns))
            rv["dSResultsRoc"][1] += (dsResults['rocBayes']/np.float(trainRuns))
            rv["dSclassifyFStime"] += (dsResults['times']/np.float(trainRuns))
            rv["dSFStime"] += (dsFStime/np.float(trainRuns))            
            rv["optParasDS"].append([rvTuning['dsSVM'],rvTuning['dsBayes'],rvTuning['dsDT']])    
               
        if algos.get('noFS',0):
            # update returnvalues                
            noFSResults = noFSResults/np.float(trainRuns)
            rv["noFSResults"] += noFSResults
            rv["noFSResultsRoc"] += noFSResultsRoc
    
            classifyNoFStime = classifyNoFStime/np.float(trainRuns)
            rv["classifyNoFStime"] += classifyNoFStime
        
        if algos.get('posSelect',0):
            starttime = time.time()
            posIndizes = sets2.makeBalancedSets(traindata, trainlabels)[2]
            posFStime = time.time() - starttime
            optClParas = classifierTuning.tuning(tuneruns, traindata[:,posIndizes], trainlabels, clParas)
            posResults = classifyNoFS(testdata[:,posIndizes],testlabels, traindata[:,posIndizes], trainlabels,optClParas)
            rv["posResults"][0] += (posResults['perfSVM']/np.float(trainRuns))
            rv["posResults"][1] += (posResults['perfBayes']/np.float(trainRuns))
            rv["posResults"][2] += (posResults['perfDT']/np.float(trainRuns))
            rv["posResultsRoc"][0] += (posResults['rocSVM']/np.float(trainRuns))
            rv["posResultsRoc"][1] += (posResults['rocBayes']/np.float(trainRuns))
            rv["posclassifyFStime"] += (posResults['times']/np.float(trainRuns))
            rv["posFStime"] += (posFStime/np.float(trainRuns))          
            rv["optParasPOS"].append(optClParas)
            rv['nFeatPos'] += len(posIndizes)/np.float(trainRuns)
        
        #rv["optThreshScoring"].append(rvTuning['optThreshScoring'])        
        #rv["optParaCombiRF"].append(rvTuning['optParaCombiRF'])
        #rv["optThreshDT"].append(rvTuning['optThreshDT'])
        #rv["optParaCombiRFE"].append(rvTuning['optParaCombiRFE'])
        #rv["optThreshDS"].append(rvTuning['optThreshDS'])

    return rv
            
    
# Classify bekommt je 3 Mengen (selektierte Mengen für die unterschiedlichen Klassifikatoren
# und optimale Parameter für jeden Algo als Dictionary übergeben   
def classify(testdata, testlabels, traindata, trainlabels, optParasSVM, optParasDT):
    
    times = np.array([0,0,0,0,0,0],dtype=np.float_) # times for fitting and classification
    
    #SVM klassifiziert
    starttime = time.time()
    print('using',optParasSVM[0], optParasSVM[1], optParasSVM[2])
    clf = SVC(C=optParasSVM[0], kernel=optParasSVM[1], gamma=optParasSVM[2], probability=True)
    clf.fit(traindata[0],trainlabels)
    scoresSVM = clf.decision_function(testdata[0])
    rocSVM = roc_auc_score(testlabels, scoresSVM) 
    times[0] = time.time() - starttime
    starttime = time.time()
    svmResults = clf.predict(testdata[0])
    print('svmResults',svmResults)
    times[1] = time.time() - starttime
    perfSVM = performance.performanceEvaluation(svmResults,testlabels)
    print('perfSVM',perfSVM)
    
    #Naive Bayes klassifiziert
    starttime = time.time()
    clf = MultinomialNB()
    clf.fit(traindata[1],trainlabels)
    scoresBayes = clf.predict_proba(testdata[1])[:,0]
    rocBayes = roc_auc_score(testlabels, scoresBayes)
    MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
    times[2] = time.time()-starttime
    starttime = time.time()
    naiveBayesResults = clf.predict(testdata[1])
    print('naiveBayesResults',naiveBayesResults)
    times[3] = time.time() - starttime

    perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels) 
    
    
    #Decision Tree klassifiziert
    starttime = time.time()
    clf = DecisionTreeClassifier(max_features=optParasDT[0],max_depth=optParasDT[1]) 
    clf.fit(traindata[2],trainlabels) 
    times[4] = time.time() - starttime
    starttime = time.time()
    decisionTreeResults = clf.predict(testdata[2])
    print('decisionTreeResults',decisionTreeResults)
    times[5] = time.time() - starttime
    perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
    print('perfDT',perfDT)
        
    print("time for fitting the SVM : " + str(times[0]) + "\ntime for SVM classification : " + str(times[1]))
    print("time for fitting Bayes: " + str(times[2]) + "\ntime for Bayes classification : " + str(times[3]))    
    print("time for fitting the decision tree: " + str(times[4]) + "\ntime for Decision Tree classification : " + str(times[5]) + "\n")    
    
    performances = {'perfSVM':perfSVM, 'rocSVM':rocSVM,
                    'perfBayes':perfBayes, 'rocBayes':rocBayes,
                    'perfDT':perfDT,  
                    'times':np.array([times[0],times[1],times[2],times[3],times[4],times[5]])
                    }    
                    
    return performances

# Classify für Feature Selection Algorithmen ohne Parameter (Gemeinsame Trainings-
# und Testmengen für alle 3 Klassifikatoren werden übergeben).  
def classifyNoFS(testdata, testlabels, traindata, trainlabels, clParas):
    
    times = np.array([0,0,0,0,0,0],dtype=np.float_) # times for fitting and classification
    
    #SVM klassifiziert
    starttime = time.time()
    clf = SVC(C=clParas['optSVMC'], kernel = clParas['optSVMkernel'], gamma=clParas['optSVMgamma'], probability=True)
    clf.fit(traindata,trainlabels)
    scoresSVM = clf.decision_function(testdata)
    rocSVM = roc_auc_score(testlabels, scoresSVM) 
    times[0] = time.time() - starttime
    starttime = time.time()
    svmResults = clf.predict(testdata)
    times[1] = time.time() - starttime
    perfSVM = performance.performanceEvaluation(svmResults,testlabels)
    
    #Naive Bayes klassifiziert
    starttime = time.time()
    clf = MultinomialNB()
    clf.fit(traindata,trainlabels)
    scoresBayes = clf.predict_proba(testdata)[:,0]
    rocBayes = roc_auc_score(testlabels, scoresBayes)
    MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
    times[2] = time.time()-starttime
    starttime = time.time()
    naiveBayesResults = clf.predict(testdata)
    times[3] = time.time() - starttime

    perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels) 
    
    #Decision Tree klassifiziert
    starttime = time.time()
    clf = DecisionTreeClassifier(max_features=clParas['optDTMaxFeatures'],max_depth=clParas['optDTMaxDepth']) # max_feature nötig?
    clf.fit(traindata,trainlabels) 
    times[4] = time.time() - starttime
    starttime = time.time()
    decisionTreeResults = clf.predict(testdata)
    times[5] = time.time() - starttime
    perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
        
    print("time for fitting the SVM : " + str(times[0]) + "\ntime for SVM classification : " + str(times[1]))
    print("time for fitting Bayes: " + str(times[2]) + "\ntime for Bayes classification : " + str(times[3]))    
    print("time for fitting the decision tree: " + str(times[4]) + "\ntime for Decision Tree classification : " + str(times[5]) + "\n")    
    
    performances = {'perfSVM':perfSVM, 'rocSVM':rocSVM,
                    'perfBayes':perfBayes, 'rocBayes':rocBayes,
                    'perfDT':perfDT,  
                    'times':np.array([times[0],times[1],times[2],times[3],times[4],times[5]])
                    }    
                    
    return performances

