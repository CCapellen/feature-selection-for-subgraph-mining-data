# -*- coding: utf-8 -*-
"""
Created on Wed May 18 10:11:05 2016

@author: Maren, Catherine

Adaption von experiment.py: Im Parametertuning kann nur der lineare Kernel für die SVM ausgewählt werden.
Dieses Modul wurde für die Experimente auf den 
Probabilistic Frequent Subtree Daten verwendet.

"""
import trainingLinearSVM
import datetime
import pickle
import scipy
import numpy as np

algos = {'noFS':0,
         'scoringV':1,
         'scoringC':0,
         'scoringF':0,
         'decisionTreeFilter':0,
         'relief':0,
         'logisticRegression':0,
         'randomForestFeatureSelection':0,
         'recursiveFeatureElimination':0,
         'svmRandomForwardWrapper':0,
         'decisionStumps':0,
         'mrmr':0,
         'geneticAlgorithm':0,
         'posSelect':0
         }
 
# Wähle Parameter für Algorithmen aus, Gamma Werte werden ignoriert
fsParas = {'scoringCThresholds':[5000,10000,15000,20000,25000],
           'scoringFThresholds':[5000,10000,15000,20000,25000],  
           'rFNumTrees':[400], 'rFD':[150,200],
           'dTmaxDepth':[None,50,60,100], 'dTThresholds':[135,150,175,200,],
           'rFEStep':[0.3], 'rFENFeaturesToSelect':[10000,5000,2000],
           'dSD':[5000,10000,15000],
           'mrmrNFeat':10}
 
# Parameter für Klassifikations Algorithmen          
clParas = {'SVMClinear':[1,2,2.5,3], 'SVMCrbf':[], 'SVMgamma':[],
           'dTMaxFeatures':[None], 'dTMaxDepth':[50,70,90]} #dtMaxFeatures muss kleiner als kleinster Threshold sein

# Wähle Datensatz aus
foldername = "Experiment2Maren/"         
dataname = 'AIDS99_connected.k1.t800features.binarize' #"AIDS99.cpk" # 'AIDS99_connected.k1.t800features.binarize'
oneOut = False # "False" entspricht "Maren"


rv = trainingLinearSVM.training(algos, fsParas, clParas, dataname, foldername, oneOut,5,5)
rv['fsParas'] = fsParas
rv['clParas'] = clParas
rv['algos'] = algos


# Bestimme Algorithmennamen für den Logfilenamen
algonames = ''
for key in algos.keys():
    if algos[key] != 0:
        algonames += key



logfilename = "sets/"+ foldername + dataname + algonames +'LINEARScoring01'
comment = ""
rv['comment'] = comment     # Kommentare zu diesem Experiment?

pickle.dump( rv, open( logfilename + "rv.p", "wb" ) )

# Erstelle Logfile
time = str(datetime.datetime.utcnow())
log = open(logfilename,"w")
log.write(comment + "\n")
log.write(time  + "\n \n")

log.write(str(algos))
log.write(str(rv))

log.flush()
log.close()
