# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 10:26:29 2016

@author: Catherine
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle


foldername = 'Experiment1Cat/ErgebnisseCat1'
noFSpickle = '/AIDS99.cpkscoringVnoFSscoringFscoringCrv.p'
pospickle = '/AIDS99.cpkposSelectrv.p'

rvNoFS = pickle.load( open( "sets/" + foldername + noFSpickle, "r" ) )
rvPOS = pickle.load( open( "sets/" + foldername + pospickle, "r" ) )


rvFS = [rvPOS]
resultsNames = ['posResults']
featNames = []
names = ['Pos']
feat_access = [True]#num_feat has to be calculated 
feat_access_name = ['nFeatPos']
roc_names = ['posResultsROC']
num_features = []      
def autolabel(rects,numbars):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/np.float(numbars), 1.05*height, '%d'%int(height),
                ha='center', va='bottom')

print(rvNoFS['setname'])
noFS = rvNoFS['noFSResults'][:,5]

ind = np.arange(3)  # the x locations for the groups
width = 0.35       # the width of the bars

for i in range(len(rvFS)):
    print('F-Scores vor und nach Feature Selektion durch ' + names[i] + '\n')
    fs = rvFS[i][resultsNames[i]][:,5]
    print(fs-noFS)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    rects1 = ax.bar(0.15 + ind, noFS, width, color='y')
    
    
    rects2 = ax.bar(0.15 + ind+width, fs, width, color='r')

    # add some
    ax.set_ylabel('F-Score')
    ax.set_title('OneOut')
    
    ax.set_xticks(ind+width)
    ax.set_xticklabels( ('SVM', 'Bayes', 'DT') )

    ax.legend( (rects1[0], rects2[0]), ('ohne Feature Selection', 'Nach Feature Selection'),bbox_to_anchor=(1.7, 0.8), loc=5 )   

    
    #autolabel(rects1)
    #autolabel(rects2)
    
    plt.show()

    if feat_access[i] and i!=9 and i!=4 and i!=10:
        fe = 0
        ge = rvFS[i][feat_access_name[i]]
        for j in range(5):
            fe += (ge[j][0][1][-1] + ge[j][1][1][-1] + ge[j][2][1][-1])
        num_features.append(fe/15.)
    elif i!=9 and i!=4 and i !=10:
        num_features.append(rvFS[i][feat_access_name[i]])
    else:
        num_features.append(-1)
    print ('num features: ' +str(num_features[-1]))

print(rvNoFS['setname'])
print('F-SCORES: (SVM, Bayes, DT)')
noFS = rvNoFS['noFSResults'][:,5]  
print( 'noFS:     ' + str(noFS) + '    ROC: ' +str(rvNoFS['noFSResultsRoc']/5.))

for i in range(len(rvFS)):
    fs = rvFS[i][resultsNames[i]][:,5]
    fsRoc = rvFS[i][resultsNames[i]+ 'Roc']
    print(names[i] + '     ' + str(fs) + '    ' + 'Roc' + str(fsRoc[0]) + ' & ' + str(fsRoc[1]) + '    ' + str(num_features[i]))

#rv['dTResultsRoc']
#rv['noFSResultsRoc']



 
