# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 14:27:46 2016

@author: Maren
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle


DTpickle = ''
noFSpickle = ''
scoringVpickle = ''

#rvNOFS = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkscoringVnoFSscoringFscoringCrv.p", "r" ) )
rv1 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkreliefrv.p", "r" ) )
#rv2 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkscoringVnoFSscoringFscoringCrv.p", "r" ) )
#rv3 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkscoringFrv.p", "r" ) )
#rv4 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkrandomForestFeatureSelectionrv.p", "r" ) )
#rv5 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkdecisionTreeFilterrv.p", "r" ) )
#rv6 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpklogisticRegressionrv.p", "r" ) )
#rv7 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkdecisionStumpsrv.p", "r" ) )
#rv8 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkgeneticAlgorithmrv.p", "r" ) )
#rv9 = pickle.load( open( "sets/Experiment1Cat/ErgebnisseCat1/AIDS99.cpkrecursiveFeatureEliminationrv.p", "r" ) )

print(rv1['reliefFStime'])
"""
print(rvNOFS['setname'])
print("\n")
noFSTime =  rvNOFS['classifyNoFStime']
print(noFSTime)

noFSFittingTimeSVM = noFSTime[0]
print(noFSFittingTimeSVM)
noFSClassifyTimeSVM = noFSTime[1]
noFSTimeSVM = noFSTime[0]+noFSTime[1]
noFSTimeBayes = (noFSTime[2]+noFSTime[3])
noFSFittingTimeDT = noFSTime[4]
noFSClassifyTimeDT = noFSTime[5]
noFSTimeDT = noFSTime[4]+noFSTime[5]
#
#rv['dTclassifyFStime']
scoringVTime = rvNOFS['classifyNoFStime']-rv1['sVclassifyFStime']
scoringCTime = rvNOFS['classifyNoFStime']-rv2['sCclassifyFStime']
scoringFTime = rvNOFS['classifyNoFStime']-rv3['sFVclassifyFStime']
dtTime = rvNOFS['classifyNoFStime']-rv5['dTclassifyFStime']
rFTime = rvNOFS['classifyNoFStime']-rv4['rFclassifyFStime']
lRTime = rvNOFS['classifyNoFStime']-rv6['lRclassifyFStime']
dSTime = rvNOFS['classifyNoFStime']-rv7['dSclassifyFStime']
rFETime = rvNOFS['classifyNoFStime']-rv9['rFEclassifyFStime']
gATime = rvNOFS['classifyNoFStime']-rv8['gaclassifyFStime']

scoringVRunTime = rv1['sVFStime']
scoringCRunTime = rv2['sCFStime']
scoringFRunTime = rv3['sFVFStime']
dtRunTime = rv5['dTFStime']
rFRunTime = rv4['rFFStime']
lRRunTime = rv6['lRFStime']
dSRunTime = rv7['dSFStime']
rFERunTime = rv9['rFEFStime']
gARunTime = rv8['gaFStime']

print("Laufzeit der FS Algorithmen: ")
print("ScoringV: ", scoringVRunTime)
print("ScoringC: ", scoringCRunTime)
print("ScoringF: ", scoringFRunTime)
print("Decision Tree: ", dtRunTime)
print("Random Forest: ", rFRunTime)
print("Logistic Regression: ", lRRunTime)
print("Decision Stumps: ", dSRunTime)
print("RFE: ", rFERunTime)
print("Genetic Algorithm: ", gARunTime)


print("Differenz der Laufzeit der Klassifikation jeweils vor und nach der FS durch die Algorithmen: ")
print("ScoringV: ", scoringVTime)
print("ScoringC: ", scoringCTime)
print("ScoringF: ", scoringFTime)
print("Decision Tree: ", dtTime)
print("Random Forest: ", rFTime)
print("Logistic Regression: ", lRTime)
print("Decision Stumps: ", dSTime)
print("RFE: ", rFETime)

scoringVFittingTimeSVM = scoringVTime[0]
scoringVClassifyTimeSVM = scoringVTime[1]
scoringVTimeSVM = scoringVTime[0]+scoringVTime[1]
scoringVTimeBayes = (scoringVTime[2]+scoringVTime[3])
scoringVFittingTimeDT = scoringVTime[4]
scoringVClassifyTimeDT = scoringVTime[5]
scoringVTimeDT = scoringVTime[4]+scoringVTime[5]

scoringCFittingTimeSVM = scoringCTime[0]
scoringCClassifyTimeSVM = scoringCTime[1]
scoringCTimeSVM = scoringCTime[0]+scoringCTime[1]
scoringCTimeBayes = (scoringCTime[2]+scoringCTime[3])
scoringCFittingTimeDT = scoringCTime[4]
scoringCClassifyTimeDT = scoringCTime[5]
scoringCTimeDT = scoringCTime[4]+scoringCTime[5]

scoringFFittingTimeSVM = scoringFTime[0]
scoringFClassifyTimeSVM = scoringFTime[1]
scoringFTimeSVM = scoringFTime[0]+scoringFTime[1]
scoringFTimeBayes = (scoringFTime[2]+scoringCTime[3])
scoringFFittingTimeDT = scoringFTime[4]
scoringFClassifyTimeDT = scoringFTime[5]
scoringFTimeDT = scoringFTime[4]+scoringFTime[5]

dtFittingTimeSVM = dtTime[0]
dtClassifyTimeSVM = dtTime[1]
dtTimeSVM = (dtTime[0]+dtTime[1])
dtTimeBayes = (dtTime[2]+dtTime[3])
dtFittingTimeDT = dtTime[4]
dtClassifyTimeDT = dtTime[5]
dtTimeDT = (dtTime[4]+dtTime[5])

rfFittingTimeSVM = rFTime[0]
rfClassifyTimeSVM = rFTime[1]
rFTimeSVM = (rFTime[0]+rFTime[1])
rFTimeBayes = (rFTime[2]+rFTime[3])
rfFittingTimeDT = rFTime[4]
rfClassifyTimeDT = rFTime[5]
rFTimeDT = (rFTime[4]+rFTime[5])

lRFittingTimeSVM = lRTime[0]
lRClassifyTimeSVM = lRTime[1]
lRTimeSVM = (lRTime[0]+lRTime[1])
lRTimeBayes = (lRTime[2]+lRTime[3])
lRFittingTimeDT = lRTime[4]
lRClassifyTimeDT = lRTime[5]
lRTimeDT = (lRTime[4]+lRTime[5])

dSFittingTimeSVM = dSTime[0]
dSClassifyTimeSVM = dSTime[1]
dSTimeSVM = (dSTime[0]+dSTime[1])
dSTimeBayes = (dSTime[2]+dSTime[3])
dSFittingTimeDT = dSTime[4]
dSClassifyTimeDT = dSTime[5]
dSTimeDT = (dSTime[4]+dSTime[5])


rFEFittingTimeSVM = rFETime[0]
rFEClassifyTimeSVM = rFETime[1]
rFETimeSVM = (rFETime[0]+rFETime[1])
rFETimeBayes = (rFETime[2]+rFETime[3])
rFEFittingTimeDT = rFETime[4]
rFEClassifyTimeDT = rFETime[5]
rFETimeDT = (rFETime[4]+rFETime[5])

gAFittingTimeSVM = gATime[0]
gAClassifyTimeSVM = gATime[1]
gATimeSVM = (gATime[0]+gATime[1])
gATimeBayes = (gATime[2]+gATime[3])
gAFittingTimeDT = gATime[4]
gAClassifyTimeDT = gATime[5]
gATimeDT = (gATime[4]+gATime[5])

#SVMtimes = np.array((scoringVTimeSVM, scoringCTimeSVM, scoringFTimeSVM, dtTimeSVM, rFTimeSVM, lRTimeSVM, dSTimeSVM, rFETimeSVM))
#SVMRanking = np.argsort(SVMtimes)
#print(SVMtimes)
#print(SVMRanking)
print("\n")
print("Verbesserung der Laufzeit der SVM durch die FS Algorithmen: ")
print("ScoringV: ", scoringVFittingTimeSVM, " (Fitting), ", scoringVClassifyTimeSVM, " (Classifying)")
print("ScoringC: ", scoringCFittingTimeSVM, " (Fitting), ", scoringCClassifyTimeSVM, " (Classifying)")
print("ScoringF: ", scoringFFittingTimeSVM, " (Fitting), ", scoringFClassifyTimeSVM, " (Classifying)")
print("Decision Tree: ", dtFittingTimeSVM, " (Fitting), ", dtClassifyTimeSVM, " (Classifying)")
print("Random Forest: ", rfFittingTimeSVM, " (Fitting), ", rfClassifyTimeSVM, " (Classifying)")
print("Logistic Regression: ", lRFittingTimeSVM, " (Fitting), ", lRClassifyTimeSVM, " (Classifying)")
print("Decision Stumps: ", dSFittingTimeSVM, " (Fitting), ", dSClassifyTimeSVM, " (Classifying)")
print("RFE: ", rFEFittingTimeSVM, " (Fitting), ", rFEClassifyTimeSVM, " (Classifying)")
print("Genetic Algorithm: ", gAFittingTimeSVM, " (Fitting), ", gAClassifyTimeSVM, " (Classifying)")

print("\n")
#Bayestimes = np.array((scoringVTimeBayes, dtTimeBayes, rFTimeBayes, lRTimeBayes, dSTimeBayes, rFETimeBayes))
#BayesRanking = np.argsort(Bayestimes)
#print(Bayestimes)
#print(BayesRanking)
print("Verbesserung der Laufzeit von Naive Bayes durch die FS Algorithmen: ")
print("ScoringV: ", scoringVTimeBayes)
print("ScoringC: ", scoringCTimeBayes)
print("ScoringF: ", scoringFTimeBayes)
print("Decision Tree: ", dtTimeBayes)
print("Random Forest: ", rFTimeBayes)
print("Logistic Regression: ", lRTimeBayes)
print("Decision Stumps: ", dSTimeBayes)
print("RFE: ", rFETimeBayes)
print("Genetic Algorithm: ", gATimeBayes)


print("\n")
#DTtimes = np.array((scoringVTimeDT, dtTimeDT, rFTimeDT, lRTimeDT, dSTimeDT, rFETimeDT))
#DTRanking = np.argsort(DTtimes)
#print(DTtimes)
#print(DTRanking)
print("Verbesserung der Laufzeit vom Decision Tree Classifier durch die FS Algorithmen: ")
print("ScoringV: ", scoringVFittingTimeDT, " (Fitting), ", scoringVClassifyTimeDT, " (Classifying)")
print("ScoringC: ", scoringCFittingTimeDT, " (Fitting), ", scoringCClassifyTimeDT, " (Classifying)")
print("ScoringF: ", scoringFFittingTimeDT, " (Fitting), ", scoringFClassifyTimeDT, " (Classifying)")
print("Decision Tree: ", dtFittingTimeDT, " (Fitting), ", dtClassifyTimeDT, " (Classifying)")
print("Random Forest: ", rfFittingTimeDT, " (Fitting), ", rfClassifyTimeDT, " (Classifying)")
print("Logistic Regression: ", lRFittingTimeDT, " (Fitting), ", lRClassifyTimeDT, " (Classifying)")
print("Decision Stumps: ", dSFittingTimeDT, " (Fitting), ", dSClassifyTimeDT, " (Classifying)")
print("RFE: ", rFEFittingTimeDT, " (Fitting), ", rFEClassifyTimeDT, " (Classifying)")
print("Genetic Algorithm: ", gAFittingTimeDT, " (Fitting), ", gAClassifyTimeDT, " (Classifying)")


ind = np.arange(1)  # the x locations for the groups
width = 0.35       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects01 = ax.bar(ind, scoringVTime[0], width, color='y')
rects02 = ax.bar(ind+width, scoringVTime[1], width, color='r')
rects03 = ax.bar(ind+2*width, scoringVTime[2], width, color='b')
rects04 = ax.bar(ind+3*width, scoringVTime[3], width, color='g')
rects05 = ax.bar(ind+4*width, scoringVTime[4], width, color='c')
rects06 = ax.bar(ind+5*width, scoringVTime[5], width, color='k')


# add some
ax.set_ylabel('Laufzeitverbesserung in s')
ax.set_title('Verbesserung der Laufzeit der Klassifikatoren durch Feature Selektion mit ScoringV')
ax.set_xticks(ind+width)

ax.legend( (rects01[0], rects02[0], rects03[0], rects04[0], rects05[0], rects06[0]), ('SVMFittingTime', 'SVMClassifyTime', 'BayesFittingTime', 'BayesClassifyTime', 'DTFittingTime', 'DTClassifyTime'),loc=5 )


plt.show()

#DT Filter
ind = np.arange(1)  # the x locations for the groups
width = 0.35       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects11 = ax.bar(ind, dtTime[0], width, color='y')
rects12 = ax.bar(ind+width, dtTime[1], width, color='r')
rects13 = ax.bar(ind+2*width, dtTime[2], width, color='b')
rects14 = ax.bar(ind+3*width, dtTime[3], width, color='g')
rects15 = ax.bar(ind+4*width, dtTime[4], width, color='c')
rects16 = ax.bar(ind+5*width, dtTime[5], width, color='k')

ax.set_ylabel('Laufzeitverbesserung in s')
ax.set_title('Verbesserung der Laufzeit der Klassifikatoren durch Feature Selektion mit Decision Tree Filter')
ax.set_xticks(ind+width)

ax.legend( (rects11[0], rects12[0], rects13[0], rects14[0], rects15[0], rects16[0]), ('SVMFittingTime', 'SVMClassifyTime', 'BayesFittingTime', 'BayesClassifyTime', 'DTFittingTime', 'DTClassifyTime'),loc=5 )


plt.show()


#Random Forest FS
ind = np.arange(1)  # the x locations for the groups
width = 0.35       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects21 = ax.bar(ind, rFTime[0], width, color='y')
rects22 = ax.bar(ind+width, rFTime[1], width, color='r')
rects23 = ax.bar(ind+2*width, rFTime[2], width, color='b')
rects24 = ax.bar(ind+3*width, rFTime[3], width, color='g')
rects25 = ax.bar(ind+4*width, rFTime[4], width, color='c')
rects26 = ax.bar(ind+5*width, rFTime[5], width, color='k')

ax.set_ylabel('Laufzeitverbesserung in s')
ax.set_title('Verbesserung der Laufzeit der Klassifikatoren durch Feature Selektion mit Random Forest FS')
ax.set_xticks(ind+width)

ax.legend( (rects11[0], rects12[0], rects13[0], rects14[0], rects15[0], rects16[0]), ('SVMFittingTime', 'SVMClassifyTime', 'BayesFittingTime', 'BayesClassifyTime', 'DTFittingTime', 'DTClassifyTime'),loc=5 )


plt.show()


#Logistic Regression
ind = np.arange(1)  # the x locations for the groups
width = 0.35       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

rects31 = ax.bar(ind, lRTime[0], width, color='y')
rects32 = ax.bar(ind+width, lRTime[1], width, color='r')
rects33 = ax.bar(ind+2*width, lRTime[2], width, color='b')
rects34 = ax.bar(ind+3*width, lRTime[3], width, color='g')
rects35 = ax.bar(ind+4*width, lRTime[4], width, color='c')
rects36 = ax.bar(ind+5*width, lRTime[5], width, color='k')

ax.set_ylabel('Laufzeitverbesserung in s')
ax.set_title('Verbesserung der Laufzeit der Klassifikatoren durch Feature Selektion mit Logistic Regression')
ax.set_xticks(ind+width)

ax.legend( (rects21[0], rects22[0], rects23[0], rects24[0], rects25[0], rects26[0]), ('SVMFittingTime', 'SVMClassifyTime', 'BayesFittingTime', 'BayesClassifyTime', 'DTFittingTime', 'DTClassifyTime'),loc=5 )


plt.show()
"""