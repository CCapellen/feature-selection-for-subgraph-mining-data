# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 10:26:29 2016

@author: Catherine
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle

foldername = 'Experiment1Maren/Ergebnisse/'

'''
noFSpickle = 'AIDS99.cpkscoringVnoFSscoringFscoringCrv.p'
scoringVpickle = 'AIDS99.cpkscoringVnoFSscoringFscoringCrv.p'
scoringFpickle = 'AIDS99.cpkscoringFrv.p'
scoringCpickle = 'AIDS99.cpkscoringCrv.p'
DTpickle = 'AIDS99.cpkdecisionTreeFilterrv.p'
MRMRpickle = 'AIDS99.cpkmrmrrv.p'
DSpickle = 'AIDS99.cpkdecisionStumpsrv.p'
RFpickle = 'AIDS99.cpkrandomForestFeatureSelectionrv2.p'
LRpickle = 'AIDS99.cpklogisticRegressionrv.p'
rfepickle = 'AIDS99.cpkrecursiveFeatureEliminationrv.p'
ReliefPickle = 'AIDS99.cpkreliefrv.p'

gAPickle = 'AIDS99.cpkgeneticAlgorithmrv.p'
'''

noFSpickle = scoringVpickle =  DTpickle = MRMRpickle = DSpickle = RFpickle = LRpickle = rfepickle = 'AIDS99.cpkscoringFscoringVdecisionTreeFilterlogisticRegressionrandomForestFeatureSelectionscoringCdecisionStumpsnoFSrecursiveFeatureEliminationrv.p'
ReliefPickle = 'AIDS99.cpkreliefnoFSlogisticRegressionrv.p'
scoringFpickle = scoringCpickle = 'AIDS99.cpkscoringCscoringFrv.p'

rvNoFS = pickle.load( open( "sets/" + foldername + noFSpickle, "rb")) 
rvScoringV = pickle.load( open( "sets/" + foldername + scoringVpickle, "rb" ) )
rvScoringF = pickle.load( open( "sets/" + foldername + scoringFpickle, "rb" ) )
rvScoringC = pickle.load( open( "sets/" + foldername + scoringCpickle, "rb" ) )
rvDT = pickle.load( open( "sets/" + foldername + DTpickle, "rb" ) )
rvMRMR = pickle.load( open( "sets/" + foldername + MRMRpickle, "rb" ) )
rvDS = pickle.load( open( "sets/" + foldername + DSpickle, "rb" ) )
rvRF = pickle.load( open( "sets/" + foldername + RFpickle, "rb" ) )
rvLR = pickle.load( open( "sets/" + foldername + LRpickle, "rb" ) )
rvRFE = pickle.load( open( "sets/" + foldername + rfepickle, "rb" ) )
rvRelief = pickle.load( open( "sets/" + foldername + ReliefPickle, "rb" ) )
#rvGA = pickle.load( open( "sets/ErgebnisseCat1/"  + gAPickle, "rb" ) )

#Fehler im MRMR Results Format:
rvMRMR['mrmrResults'] = rvMRMR['mrmrResults'][:3,:]


rvFS = [rvScoringV, rvScoringF,rvScoringC,rvDT,rvMRMR,rvDS, rvRF, rvLR,rvRFE,
        rvRelief] #,rvGA]
resultsNames = ['scoringVResults','scoringFResults','scoringCResults','dTResults',
                'mrmrResults','dSResults', 'rFResults', 'lRResults','rFEResults', 
                'reliefResults','gaResults']
featNames = []
names = ['ScoringV','ScoringF','ScoringC','Decision Tree', 'MRMR', 'Decision Stumps',
         'Random Forest', 'Logistic Regression', 'Recursive Feature Eliminination', 
         'Relief','Genetischer Wrapper']
feat_access = [False,True,True,True,True, 
               True,True, False, True,True,True]#num_feat has to be calculated 
feat_access_name = ['nFeatScoringV','optParasScoringF','optParasScoringC',
                    'optParasDT','optParasMrmr','optParasDS', 'optParasRF',
                    'nFeatLR', 'optParasRFE','','optParasGA']
num_features = []      
def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),
                ha='center', va='bottom')

print(rvNoFS['setname'])
noFS = rvNoFS['noFSResults'][:,5]

ind = np.arange(3)  # the x locations for the groups
width = 0.35       # the width of the bars

for i in range(len(rvFS)-1):
 
    fs = rvFS[i][resultsNames[i]][:,5]
    print(fs-noFS)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    rects1 = ax.bar(0.15 + ind, noFS, width, color='y')
    
    
    rects2 = ax.bar(0.15 + ind+width, fs, width, color='r')

    # add some
    ax.set_ylabel('F-Score')
    ax.set_ylim([0,0.7])
    ax.set_title('WithOnes')#
    print('F-Scores vor und nach Feature Selektion durch ' + names[i] + '\n')
    ax.set_xticks(ind+width)
    ax.set_xticklabels( ('SVM', 'Bayes', 'DT') )

    ax.legend( (rects1[0], rects2[0]), ('ohne Feature Selection', 'Nach Feature Selection'),bbox_to_anchor=(1.7, 0.8), loc=5 )   
    
    
    #autolabel(rects1)
    #autolabel(rects2)
    
    plt.show()

    if feat_access[i] and i!=9 and i!=4 and i!=10:
        fe = 0
        ge = rvFS[i][feat_access_name[i]]
        for j in range(5):
            fe += (ge[j][0][1][-1] + ge[j][1][1][-1] + ge[j][2][1][-1])
        num_features.append(fe/15.)
    elif i!=9 and i!=4 and i !=10:
        num_features.append(rvFS[i][feat_access_name[i]])
    else:
        num_features.append(-1)
    print ('num features: ' +str(num_features[-1]))

print(rvNoFS['setname'])
print('F-SCORES: (SVM, Bayes, DT)')
noFS = rvNoFS['noFSResults'][:,5]
print( 'noFS:     ' + str(noFS)+ '    ROC: ' +str(rvNoFS['noFSResultsRoc']/5.))

for i in range(len(rvFS)):
    fs = rvFS[i][resultsNames[i]][:,5]
    fsRoc = rvFS[i][resultsNames[i]+ 'Roc']
    print(names[i] + '     ' + str(fs) + '    ' + 'Roc' + str(fsRoc) + '    ' + str(num_features[i]))

#rv['dTResultsRoc']
#rv['noFSResultsRoc']
 
