# -*- coding: utf-8 -*-
"""
Created on Tue May 17 13:35:52 2016

@author: Maren, Cat
"""
import numpy as np
import scipy.sparse as sp
import scipy
import featureSelection
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import KFold
import performance

def classifyTuning(resultsSVM, resultsBayes,resultsDT, j, clParas, tuneruns, testdata, testlabels, traindata, trainlabels):

    #SVM klassifiziert
    i = 0
    for c in clParas.get('SVMClinear'):         
        clf = SVC(C=c, kernel = 'linear')
        clf.fit(traindata,trainlabels)
        svmClass = clf.predict(testdata)
        fscore = performance.performanceEvaluation(svmClass,testlabels)[5]
        resultsSVM[i][j] += (fscore/np.float(tuneruns))            
        i += 1      
    for c in clParas.get('SVMCrbf'):
        for g in clParas.get('SVMgamma'): 
            clf = SVC(C=c, kernel = 'rbf', gamma=g)
            clf.fit(traindata,trainlabels)
            svmClass = clf.predict(testdata)
            fscore = performance.performanceEvaluation(svmClass,testlabels)[5]
            resultsSVM[i][j] += (fscore/np.float(tuneruns))            
            i += 1            
    
    #Naive Bayes klassifiziert
    clf = MultinomialNB()
    clf.fit(traindata,trainlabels)
    MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
    naiveBayesClass = clf.predict(testdata)
    fscore = performance.performanceEvaluation(naiveBayesClass, testlabels)[5]
    resultsBayes[0][j] += (fscore/np.float(tuneruns))   
    
    #Decision Tree klassifiziert
    i=0
    for maxF in clParas.get('dTMaxFeatures'):
        for maxD in clParas.get('dTMaxDepth'):
            clf = DecisionTreeClassifier(max_features=maxF, max_depth=maxD) # max_feature nötig?
            clf.fit(traindata,trainlabels) 
            decisionTreeResults = clf.predict(testdata)
            fscore = performance.performanceEvaluation(decisionTreeResults, testlabels)[5]
            resultsDT[i][j] += (fscore/np.float(tuneruns))
            i += 1  
    
    # Performanzen wurden an entsprechende Stellen in results hinzuaddiert
    return resultsSVM, resultsBayes, resultsDT

# Um Indizes von 2-dim Parameterkombinationen zu finden
def findIndis(indi, widthArray):
    i = int(indi/widthArray)
    j = indi - i*widthArray
    return i,j

# Um Indizes für FS-Algos mit 1 Parameter zu finden
def findOptParameters(resultsSVM, resultsDT, resultsBayes, svmClinearlen, svmCrbflen, dtPara2len):
    indi = np.argmax(resultsSVM)
    if indi < (svmClinearlen*resultsSVM.shape[1]):
        iASVM,jSVM = findIndis(indi,resultsSVM.shape[1])
        iBSVM = 0 #linear
        iCSVM = 0 #egal
    else:
        iSVM, jSVM = findIndis(indi-(svmClinearlen*resultsSVM.shape[1]),resultsSVM.shape[1])
        iASVM, iCSVM = findIndis(iSVM,  svmCrbflen)
        iBSVM = 1 #rbf
    
    iDT, jDT = findIndis(np.argmax(resultsDT), resultsDT.shape[1])
    iB, jB = findIndis(np.argmax(resultsBayes), resultsBayes.shape[1])     
     
    
    iADT,iBDT = findIndis(iDT,dtPara2len)
    
    return iASVM, iBSVM, iCSVM, jSVM, iADT, iBDT, jDT, iB, jB

# Um Indizes für FS-Algos mit 2 Parametern zu finden
def findOptParameters2(resultsSVM, resultsDT, resultsBayes, svmClinearlen, svmCrbflen, dtPara2len, fsPara2len):
    iSVM, jSVM = findIndis(np.argmax(resultsSVM), resultsSVM.shape[1])
    if iSVM < svmClinearlen:
        iASVM = iSVM
        iBSVM = 0 #linear
        iCSVM = 0 #egal
    else: 
        iASVM, iCSVM = findIndis(iSVM-svmClinearlen, svmCrbflen)  
        iBSVM = 1 #rbf
    
    jASVM,jBSVM = findIndis(jSVM,fsPara2len)  

    iDT, jDT = findIndis(np.argmax(resultsDT), resultsDT.shape[1])    
    iADT,iBDT = findIndis(iDT,dtPara2len)
    jADT,jBDT = findIndis(jDT,fsPara2len)
    
    iB, jB = findIndis(np.argmax(resultsBayes), resultsBayes.shape[1])      
    jAB, jBB = findIndis(jB,fsPara2len)
    
    return iASVM, iBSVM, iCSVM, jASVM, jBSVM, iADT, iBDT, jADT,jBDT, iB, jAB, jBB
    

def tuning(tuneRuns, data, labels, algos, fsParas, clParas):

    #Trainings- und Testmenge für das Tuning werden erstellt
    n_samples, n_features = data.shape    
    numCV = tuneRuns
    cv = KFold(n_samples, n_folds=numCV, shuffle=True)

    # Arrays zum Speichern der Ergebnisse erstellen
    nClParaKombisSVM = len(clParas.get('SVMClinear')) + len(clParas.get('SVMCrbf'))*len(clParas.get('SVMgamma')) 
    nClParaKombisDT = len(clParas.get('dTMaxFeatures'))*len(clParas.get('dTMaxDepth'))
 
    if algos.get('scoringC',0): 
        scoringCThresholds = fsParas.get('scoringCThresholds')
        scoringCMaxThreshold = np.max(scoringCThresholds)
        scoringCResultsSVM = np.zeros((nClParaKombisSVM,len(scoringCThresholds)))  
        scoringCResultsDT = np.zeros((nClParaKombisDT,len(scoringCThresholds)))  
        scoringCResultsBayes = np.zeros((1,len(scoringCThresholds)))  
 
    if algos.get('scoringF',0): 
        scoringFThresholds = fsParas.get('scoringFThresholds')
        scoringFMaxThreshold = np.max(scoringFThresholds)
        scoringFResultsSVM = np.zeros((nClParaKombisSVM,len(scoringFThresholds)))  
        scoringFResultsDT = np.zeros((nClParaKombisDT,len(scoringFThresholds)))  
        scoringFResultsBayes = np.zeros((1,len(scoringFThresholds)))      
     
    if algos.get('randomForestFeatureSelection',0):
        rfThresholds = fsParas.get('rFD')
        rfMaxThreshold = np.max(rfThresholds)
        rfNumTrees = fsParas.get('rFNumTrees')
        rfResultsSVM = np.zeros((nClParaKombisSVM,len(rfNumTrees)*len(rfThresholds)))
        rfResultsDT = np.zeros((nClParaKombisDT,len(rfNumTrees)*len(rfThresholds)))
        rfResultsBayes = np.zeros((1,len(rfNumTrees)*len(rfThresholds)))
    
    if algos.get('decisionTreeFilter',0):   
        dtMaxDepth = fsParas.get('dTmaxDepth')
        dtThresholds = fsParas.get('dTThresholds')
        dtMaxThreshold = np.max(dtThresholds)
        dtResultsSVM = np.zeros((nClParaKombisSVM,len(dtMaxDepth)*len(dtThresholds)))
        dtResultsDT = np.zeros((nClParaKombisDT,len(dtMaxDepth)*len(dtThresholds)))
        dtResultsBayes = np.zeros((1,len(dtMaxDepth)*len(dtThresholds)))

    if algos.get('recursiveFeatureElimination',0):
        rfeStep = fsParas.get('rFEStep')
        rfeNFeaturesToSelect = fsParas.get('rFENFeaturesToSelect')
        rfeMaxNFeaturesToSelect = np.max(rfeNFeaturesToSelect)
        rfeResultsSVM = np.zeros((nClParaKombisSVM,len(rfeStep)*len(rfeNFeaturesToSelect)))
        rfeResultsDT = np.zeros((nClParaKombisDT,len(rfeStep)*len(rfeNFeaturesToSelect)))
        rfeResultsBayes = np.zeros((1,len(rfeStep)*len(rfeNFeaturesToSelect)))        
        
        
    if algos.get('decisionStumps',0):   
        dsThresholds = fsParas.get('dSD')
        dsMaxThreshold = np.max(dsThresholds)
        dsResultsSVM = np.zeros((nClParaKombisSVM,len(dsThresholds)))
        dsResultsDT = np.zeros((nClParaKombisDT, len(dsThresholds)))
        dsResultsBayes = np.zeros((1,len(dsThresholds)))
    """
    if algos.get('svmRandomForwardWrapper',0):
        ####TODO#####
        sVMResults = np.zeros(len(sVMParaCombis))
    """
    
    # dictionary zum Zurückgeben der optimalen Parameterkombinationen
    optParas = {}    
    
    
    for train, test in cv:
        #Trainings- und Testsets für jew. Durchlauf
        traindata = data[train]
        trainlabels = labels[train]
        testdata = data[test]
        testlabels = labels[test]        
        
        #Passendes Format  #vielleicht früher?
        testdata = testdata.tocsc()  
        traindata = traindata.tocsc()
        #testlabels = sp.csc_matrix(testlabels)
 
        #Tuning für ScoringC
        if algos.get('scoringC',0): 
            j = 0
            scoringIndices = featureSelection.scoringChi2(traindata, trainlabels, scoringCMaxThreshold)
            for thresh in scoringCThresholds:                
                scoringSelectedTestdata = testdata[:,scoringIndices[:thresh]]
                scoringSelectedTraindata = traindata[:,scoringIndices[:thresh]]
                scoringCResultsSVM, scoringCResultsBayes, scoringCResultsDT = classifyTuning(scoringCResultsSVM, scoringCResultsBayes,scoringCResultsDT,  j, clParas, tuneRuns, scoringSelectedTestdata, testlabels, scoringSelectedTraindata, trainlabels)
                j += 1
                             
        #Tuning für ScoringF
        if algos.get('scoringF',0):  
            j = 0
            scoringIndices = featureSelection.scoringFValue(traindata, trainlabels, scoringFMaxThreshold)
            for thresh in scoringFThresholds:                
                scoringSelectedTestdata = testdata[:,scoringIndices[:thresh]]
                scoringSelectedTraindata = traindata[:,scoringIndices[:thresh]]
                scoringFResultsSVM, scoringFResultsBayes , scoringFResultsDT= classifyTuning(scoringFResultsSVM, scoringFResultsBayes, scoringFResultsDT, j, clParas, tuneRuns, scoringSelectedTestdata, testlabels, scoringSelectedTraindata, trainlabels)
                j += 1
        
        #Tuning für Random Forest FS
        if algos.get('randomForestFeatureSelection',0):
            j = 0
            for numTrees in rfNumTrees:
                rFIndices = featureSelection.randomForestFS(traindata, trainlabels, numTrees, rfMaxThreshold)
                for d in rfThresholds:                
                    rfSelectedTestdata = testdata[:,rFIndices[:d]]
                    rfSelectedTraindata = traindata[:,rFIndices[:d]]
                    rfResultsSVM, rfResultsBayes, rfResultsDT= classifyTuning(rfResultsSVM, rfResultsBayes, rfResultsDT, j, clParas, tuneRuns, rfSelectedTestdata, testlabels, rfSelectedTraindata, trainlabels)               
                    j += 1
                

        #Tuning für Decision Tree FS  
        if algos.get('decisionTreeFilter',0):
            j = 0
            for depth in dtMaxDepth:
                dTIndices = featureSelection.decisionTreeFilter(traindata, trainlabels, depth, dtMaxThreshold)
                for thresh in dtThresholds:                           
                    dtSelectedTestdata = testdata[:,dTIndices[:thresh]]
                    dtSelectedTraindata = traindata[:,dTIndices[:thresh]]
                    dtResultsSVM, dtResultsBayes, dtResultsDT= classifyTuning(dtResultsSVM, dtResultsBayes, dtResultsDT, j, clParas, tuneRuns, dtSelectedTestdata, testlabels, dtSelectedTraindata, trainlabels)
                    j += 1
    
        
        #Tuning für Recursive Feature Elimination
        if algos.get('recursiveFeatureElimination',0):
            j = 0
            for step in rfeStep:
                rFEIndices = featureSelection.recursiveFeatureElimination(traindata, trainlabels, step, rfeMaxNFeaturesToSelect)
                for nFeat in rfeNFeaturesToSelect:               
                    rFEIndicesSmaller = featureSelection.recursiveFeatureElimination(traindata[:,rFEIndices] , trainlabels, step, nFeat)
                    rFESelectedTestdataSmaller = testdata[:,rFEIndicesSmaller]
                    rFESelectedTraindataSmaller = traindata[:,rFEIndicesSmaller] 
                    rfeResultsSVM, rfeResultsBayes, rfeResultsDT= classifyTuning(rfeResultsSVM, rfeResultsBayes, rfeResultsDT, j, clParas, tuneRuns,rFESelectedTestdataSmaller, testlabels, rFESelectedTraindataSmaller, trainlabels)
                    j += 1
 
            
        #Tuning für Decision Stumps
        if algos.get('decisionStumps',0):
            j = 0
            dSIndices = featureSelection.decisionStumps(traindata, trainlabels, dsMaxThreshold)
            for d in dsThresholds:               
                dsSelectedTestdata = testdata[:,dSIndices[:d]]
                dsSelectedTraindata = traindata[:,dSIndices[:d]]
                dsResultsSVM, dsResultsBayes, dsResultsDT= classifyTuning(dsResultsSVM, dsResultsBayes, dsResultsDT, j, clParas, tuneRuns,dsSelectedTestdata, testlabels, dsSelectedTraindata, trainlabels)
                j += 1
           
        """
        #Tuning für SVM Wrapper
        if algos.get('svmRandomForwardWrapper',0):
            #####TODO######
            j = 0
            for sVMParaCombi in sVMParaCombis:
                sVMIndices = featureSelection.svmRandomForwardWrapper(traindata, trainlabels, sVMParaCombi[0], sVMParaCombi[1])
                sVMSelectedTestdata = testdata[:,sVMIndices]
                sVMSelectedTraindata = traindata[:,sVMIndices]
                sVMResultsSVM, sVMResultsBayes, sVMResultsDT= classify(sVMSelectedTestdata, testlabels, sVMSelectedTraindata, trainlabels)
            
                results = (sVMResultsSVM[5]+sVMResultsBayes[5]+sVMResultsDT[5])/3
                results /= tuneRuns
                
                sVMResults[j] += results
                j += 1    
        """
            
    # Die optimalen Parameter werden ermittelt und ins dictionary gespeichert
            
    # Hilfsvariablen
    dtPara2len = len(clParas.get('dTMaxDepth'))
    svmClinear = clParas.get('SVMClinear')
    svmCrbf = clParas.get('SVMCrbf')
    svmGamma = clParas.get('SVMgamma')
    svmClinearlen = len(svmClinear)
    svmCrbflen = len(svmCrbf)
    dtPara1 = clParas.get('dTMaxFeatures')
    dtPara2 = clParas.get('dTMaxDepth')
    
    # Scoring C
    if algos.get('scoringC',0):   
        #print(scoringCResultsSVM)
        iASVM, iBSVM, iCSVM, jSVM, iADT, iBDT, jDT, iB, jB = findOptParameters(scoringCResultsSVM, scoringCResultsDT, scoringCResultsBayes, svmClinearlen, svmCrbflen, dtPara2len)
        if iBSVM == 0:
            optParas['scoringCSVM'] = [svmClinear[iASVM],'linear',1],[scoringCThresholds[jSVM]]  
        else:
            optParas['scoringCSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]],[scoringCThresholds[jSVM]]        
        optParas['scoringCDT'] = [dtPara1[iADT],dtPara2[iBDT]],[scoringCThresholds[jDT]]
        optParas['scoringCBayes'] = [iB],[scoringCThresholds[jB]]

    # Scoring F
    if algos.get('scoringF',0): 
        iASVM, iBSVM, iCSVM, jSVM, iADT, iBDT, jDT, iB, jB = findOptParameters(scoringFResultsSVM, scoringFResultsDT, scoringFResultsBayes,svmClinearlen, svmCrbflen, dtPara2len)
        if iBSVM == 0:
            optParas['scoringFSVM'] = [svmClinear[iASVM],'linear',1],[scoringFThresholds[jSVM]]  
        else:
            optParas['scoringFSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]],[scoringFThresholds[jSVM]]    
        optParas['scoringFDT'] = [dtPara1[iADT],dtPara2[iBDT]],[scoringFThresholds[jDT]]
        optParas['scoringFBayes'] = [iB],[scoringFThresholds[jB]]
     
      
    # Random Forest FS
    if algos.get('randomForestFeatureSelection',0):
        fsPara2len = len(rfThresholds)
        iASVM, iBSVM, iCSVM, jASVM, jBSVM, iADT, iBDT, jADT,jBDT, iB, jAB, jBB = findOptParameters2(rfResultsSVM, rfResultsDT, rfResultsBayes, svmClinearlen, svmCrbflen, dtPara2len, fsPara2len)
        if iBSVM == 0:
            optParas['rfSVM'] = [svmClinear[iASVM],'linear',1],[rfNumTrees[jASVM], rfThresholds[jBSVM]]  
        else:
            optParas['rfSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]],[rfNumTrees[jASVM], rfThresholds[jBSVM]]
        optParas['rfDT'] = [dtPara1[iADT],dtPara2[iBDT]],[rfNumTrees[jADT], rfThresholds[jBSVM]]
        optParas['rfBayes'] = [iB],[rfNumTrees[jAB],rfThresholds[jBB]]
        
    #Decision Tree FS  
    if algos.get('decisionTreeFilter',0):
        print('dtResultsSVM' + str(dtResultsSVM))
        print('dtResultsDT' + str(dtResultsDT))
        print('dtResultsBayes' + str(dtResultsBayes))
        fsPara2len = len(dtThresholds)
        iASVM, iBSVM, iCSVM, jASVM, jBSVM, iADT, iBDT, jADT,jBDT, iB, jAB, jBB = findOptParameters2(dtResultsSVM, dtResultsDT, dtResultsBayes, svmClinearlen, svmCrbflen,  dtPara2len, fsPara2len)
        if iBSVM == 0:
            optParas['dtSVM'] = [svmClinear[iASVM],'linear',1],[dtMaxDepth[jASVM], dtThresholds[jBSVM]]  
        else:
            optParas['dtSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]],[dtMaxDepth[jASVM], dtThresholds[jBSVM]]  
        optParas['dtDT'] = [dtPara1[iADT],dtPara2[iBDT]],[dtMaxDepth[jADT], dtThresholds[jBSVM]]
        optParas['dtBayes'] = [iB],[dtMaxDepth[jAB],dtThresholds[jBB]]

   
    #Recursive Feature Elimination
    if algos.get('recursiveFeatureElimination',0):
        print('rfeResultsSVM' + str(rfeResultsSVM))
        print('rfeResultsDT' + str(rfeResultsDT))
        print('rfeResultsBayes' + str(rfeResultsBayes))
        fsPara2len = len(rfeNFeaturesToSelect)
        iASVM, iBSVM, iCSVM, jASVM, jBSVM, iADT, iBDT, jADT,jBDT, iB, jAB, jBB = findOptParameters2(rfeResultsSVM, rfeResultsDT, rfeResultsBayes, svmClinearlen, svmCrbflen, dtPara2len, fsPara2len)
        if iBSVM == 0:
            optParas['rfeSVM'] = [svmClinear[iASVM],'linear',1],[rfeStep[jASVM], rfeNFeaturesToSelect[jBSVM]]
        else:
            optParas['rfeSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]], [rfeStep[jASVM], rfeNFeaturesToSelect[jBSVM]]   
        optParas['rfeDT'] = [dtPara1[iADT],dtPara2[iBDT]],[rfeStep[jADT], rfeNFeaturesToSelect[jBDT]]
        optParas['rfeBayes'] = [iB],[rfeStep[jAB], rfeNFeaturesToSelect[jBB]]
        optParas['rfeSteps'] = [rfeStep[jASVM],rfeStep[jAB], rfeStep[jADT]]
        optParas['rfeNFeat'] = [rfeNFeaturesToSelect[jBSVM],rfeNFeaturesToSelect[jBB],rfeNFeaturesToSelect[jBDT]]
 
    #Decision Stumps
    if algos.get('decisionStumps',0):
        iASVM, iBSVM, iCSVM, jSVM, iADT, iBDT, jDT, iB, jB = findOptParameters(dsResultsSVM, dsResultsDT, dsResultsBayes, svmClinearlen, svmCrbflen,dtPara2len)
        if iBSVM == 0:
            optParas['dsSVM'] = [svmClinear[iASVM],'linear',1],[dsThresholds[jSVM]]  
        else:
            optParas['dsSVM'] = [svmCrbf[iASVM],'rbf',svmGamma[iCSVM]],[dsThresholds[jSVM]]      
        optParas['dsDT'] = [dtPara1[iADT],dtPara2[iBDT]],[dsThresholds[jDT]]
        optParas['dsBayes'] = [iB],[dsThresholds[jB]]

   
    #Tuning für SVM Wrapper
    if algos.get('svmRandomForwardWrapper',0):
        pass ######TODO######

    return optParas


"""
testdata = scipy.io.mmread('sets/testdata1')
traindata = scipy.io.mmread('sets/traindata1')
trainlabels = scipy.io.mmread('sets/trainlabels1')
    

traindata = traindata.tocsc()
trainlabels = trainlabels.todense() 
trainlabels = np.ravel(trainlabels)
    

rv = tuning(2, traindata, trainlabels, {'scoringV':0,
     'scoringC':0,
     'scoringF':0,
     'decisionTreeFilter':0,
     'relief':0,
     'logisticRegression':0,
     'randomForestFeatureSelection':1,
     'recursiveFeatureElimination':0,
     'svmRandomForwardWrapper':0,
     'decisionStumps':0    
     }, {'scoringCThresholds':[10,20],
           'scoringFThresholds':[10,20],  
           'rFNumTrees':[5,10,15], 'rFD':[100,150,200,500],
           'dTThresholds':[5,100,200,300],'dTmaxDepth':[None,50],
           'rFEStep':[0.1,0.3], 'rFENFeaturesToSelect':[5000,1000],
           'dSD':[5000,10000,15000]}, {'SVMClinear':[0.5,1], 'SVMCrbf':[8,3], 'SVMgamma':[0.1,0.3],
           'dTMaxFeatures':[None], 'dTMaxDepth':[20,50]})
         
print(rv)
"""