# -*- coding: utf-8 -*-
"""
Created on Fri May 27 14:13:22 2016

@author: Maren
"""
import numpy as np
import scipy.sparse as sp

"""
Alg. Relief, der für ein Trainingsset eine Feature Selection durchführt 
und die Indizes der ausgewählten Features zurückgibt.

Parameter:
m = Anzahl Iterationen
d = Threshold
"""

def relief(traindata, trainlabels, m):
    weights = np.zeros((traindata.shape[1]))

    for i in range(m):
        randExample = np.random.randint(0,traindata.shape[0])
        
        #bestimme nearest Hit und nearest Miss
        minDistanceNH = float("inf")
        minDistanceNM = float("inf")
        nearestHit = 0
        nearestMiss = 0
        for ex in range(traindata.shape[0]):
            dataSum = traindata[ex,:]+traindata[randExample,:]
            distance = 0
            for f in range(traindata.shape[1]):
                if(dataSum[0,f]==1):
                    distance += 1
            if (distance <= minDistanceNH and trainlabels[ex] == trainlabels[randExample]):
                minDistanceNH = distance
                nearestHit = ex
            if (distance <= minDistanceNM and trainlabels[ex] != trainlabels[randExample]):
                minDistanceNM = distance
                nearestMiss = ex
        
        #aktualisiere Gewichte
        for f in range(traindata.shape[1]):
            weights[f] += (diff(traindata[randExample,f], traindata[nearestMiss,f])/(traindata.shape[0]*m)) - (diff(traindata[randExample,f], traindata[nearestHit,f])/(traindata.shape[0]*m))
    
    indices = np.argsort(weights)
    
    return indices


#Berechnet die Hamming-Distanz zweier Booleans    
def diff(x,y):
    if(x==y):
        return 0
    else:
        return 1