# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:19:07 2016

@author: Maren, Catherine

"""

import numpy as np
import scipy.sparse as sp
import scipy

from sklearn import cross_validation
from sklearn import datasets
from sklearn import svm
from sklearn.cross_validation import train_test_split
import time
                         
"""
Erzeugt kleinere Sets mit weniger Beispielen und Features,
die von Relief und Mrmr verwendet werden können.

Erzeugt außerdem in makeBalanced Sets, die Features, die Pos und Equal verwenden

"""
def produceSmallerSets(traindata, trainlabels, n):
    
    oldIndis = []
    
    #Zufällige n Indizes
    indis = np.array(range(traindata.shape[0]))
    np.random.shuffle(indis)
    indis = indis[:n]
    
    traindata = traindata[indis]
    smallerTrainlabels = trainlabels[indis]
    #traindata = traindata.todense() 
    
    #finde Nullspalten
    newIndi = 0
    for feature in range(traindata.shape[1]):
        for example in range(traindata.shape[0]):
            one = 0
            if(traindata[example,feature]==1):
                one = 1
                break

        if (one==1):
            oldIndis.append(feature)
            newIndi += 1
    
    oldIndis = np.array(oldIndis)
    smallerTraindata = traindata[:,oldIndis]
    
    #smallerTraindata = smallerTraindata.tocsc()
    
    return smallerTraindata, smallerTrainlabels, oldIndis
    
    
def produceSmallerSets2(traindata, trainlabels, n):
    indis = np.array(range(traindata.shape[0]))
    np.random.shuffle(indis)
    indis = indis[:n]
    oldIndis = []    
    
    traindata = traindata[indis]
    smallerTrainlabels = trainlabels[indis]

    #finde Nullspalten
    for feature in range(traindata.shape[1]):
        if traindata[:,feature].data.shape[0] != 0:
            oldIndis.append(feature)
    smallerTraindata = traindata[:,oldIndis]
    
    return smallerTraindata, smallerTrainlabels, oldIndis
            

def makeBalancedSets(traindata, trainlabels):
    balance = 0  # what percentage of negative examples should be added to the positive ones?
    posindis = sp.csc_matrix(trainlabels.reshape(-1,1)).indices
    negindis = np.random.choice(range(traindata.shape[1]),balance*len(posindis))
    
    indis = np.concatenate((posindis,negindis))
    oldIndis = []    
    
    traindata = traindata[indis]
    smallerTrainlabels = trainlabels[indis]

    #finde Nullspalten
    for feature in range(traindata.shape[1]):
        if traindata[:,feature].data.shape[0] != 0:
            oldIndis.append(feature)
    smallerTraindata = traindata[:,oldIndis]
    
    return smallerTraindata, smallerTrainlabels, oldIndis

















