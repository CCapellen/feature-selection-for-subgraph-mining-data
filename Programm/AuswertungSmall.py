# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 10:26:29 2016

@author: Catherine
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle

foldername = 'Experiment1Maren/Ergebnisse'
scoringFpickle = 'SMALL/AIDS99.cpkscoringFSMALLrv.p'
scoringCpickle = 'SMALL/AIDS99.cpkscoringCSMALLrv.p'
DTpickle = 'SMALL/AIDS99.cpkdecisionTreeFilterSMALLrv.p'
MRMRpickle = 'SMALL/AIDS99.cpkmrmrSMALLrv.p'
DSpickle = 'SMALL/AIDS99.cpkdecisionStumpsSMALLrv.p'
RFpickle = 'SMALL/AIDS99.cpkrandomForestFeatureSelectionSMALLrv.p'
rfepickle = 'SMALL/AIDS99.cpkrecursiveFeatureEliminationSMALLrv.p'


rvRFE = pickle.load( open( "sets/" + foldername + rfepickle, "r" ) )
rvScoringF = pickle.load( open( "sets/" + foldername + scoringFpickle, "r" ) )
rvScoringC = pickle.load( open( "sets/" + foldername + scoringCpickle, "r" ) )
rvDT = pickle.load( open( "sets/" + foldername + DTpickle, "r" ) )
rvMRMR = pickle.load( open( "sets/" + foldername + MRMRpickle, "r" ) )
rvDS = pickle.load( open( "sets/" + foldername + DSpickle, "r" ) )
rvRF = pickle.load( open( "sets/" + foldername + RFpickle, "r" ) )

foldername = 'Experiment1Cat/ErgebnisseCat1'
noFSpickle = '/AIDS99.cpkscoringVnoFSscoringFscoringCrv.p'
scoringVpickle = '/AIDS99.cpkscoringVnoFSscoringFscoringCrv.p'
gAPickle = '/AIDS99.cpkgeneticAlgorithmrv.p'
LRpickle = '/AIDS99.cpklogisticRegressionrv.p'
ReliefPickle = '/AIDS99.cpkreliefrv.p'

'''
rfepickle = '/AIDS99.cpkrecursiveFeatureEliminationrv.p'
scoringFpickle = '/AIDS99.cpkscoringFrv.p'
scoringCpickle = '/AIDS99.cpkscoringCrv.p'
DTpickle = '/AIDS99.cpkdecisionTreeFilterrv.p'
MRMRpickle = '/AIDS99.cpkmrmrrv.p'
DSpickle = '/AIDS99.cpkdecisionStumpsrv.p'
RFpickle = '/AIDS99.cpkrandomForestFeatureSelectionrv2.p'

'''
"""
noFSpickle = scoringVpickle = scoringFpickle = scoringCpickle = DTpickle = MRMRpickle = DSpickle = RFpickle = LRpickle = rfepickle = 'AIDS99.cpkscoringFscoringVdecisionTreeFilterlogisticRegressionrandomForestFeatureSelectionscoringCdecisionStumpsnoFSrecursiveFeatureEliminationrv.p'
ReliefPickle = 'AIDS99.cpkreliefnoFSlogisticRegression.p'
"""
print("hi")
rvNoFS = pickle.load( open( "sets/" + foldername + noFSpickle, "r" ) )
rvScoringV = pickle.load( open( "sets/" + foldername + scoringVpickle, "r" ) )

rvLR = pickle.load( open( "sets/" + foldername + LRpickle, "r" ) )

rvRelief = pickle.load( open( "sets/" + foldername + ReliefPickle, "r" ) )
rvGA = pickle.load( open( "sets/" + foldername + gAPickle, "r" ) )

#Fehler im MRMR Results Format:
rvMRMR['mrmrResults'] = rvMRMR['mrmrResults'][:3,:]


rvFS = [rvScoringV, rvScoringF,rvScoringC,rvDT,rvMRMR,rvDS, rvRF, rvLR,rvRFE,
        rvRelief,rvGA]
resultsNames = ['scoringVResults','scoringFResults','scoringCResults','dTResults',
                'mrmrResults','dSResults', 'rFResults', 'lRResults','rFEResults', 
                'reliefResults','gaResults']
featNames = []
names = ['ScoringV','ScoringF','ScoringC','Decision Tree', 'MRMR', 'Decision Stumps',
         'Random Forest', 'Logistic Regression', 'Recursive Feature Eliminination', 
         'Relief','Genetischer Wrapper']
feat_access = [False,True,True,True,True, 
               True,True, False, True,True,True]#num_feat has to be calculated 
feat_access_name = ['nFeatScoringV','optParasScoringF','optParasScoringC',
                    'optParasDT','optParasMrmr','optParasDS', 'optParasRF',
                    'nFeatLR', 'optParasRFE','','optParasGA']
roc_names = []
num_features = []      
def autolabel(rects,numbars):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/np.float(numbars), 1.05*height, '%d'%int(height),
                ha='center', va='bottom')

print(rvNoFS['setname'])
noFS = rvNoFS['noFSResults'][:,5]

ind = np.arange(3)  # the x locations for the groups
width = 0.35       # the width of the bars

for i in range(len(rvFS)):
    print('F-Scores vor und nach Feature Selektion durch ' + names[i] + '\n')
    fs = rvFS[i][resultsNames[i]][:,5]
    print(fs-noFS)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    rects1 = ax.bar(0.15 + ind, noFS, width, color='y')
    
    
    rects2 = ax.bar(0.15 + ind+width, fs, width, color='r')

    # add some
    ax.set_ylabel('F-Score')
    ax.set_title('OneOut')
    
    ax.set_xticks(ind+width)
    ax.set_xticklabels( ('SVM', 'Bayes', 'DT') )

    ax.legend( (rects1[0], rects2[0]), ('ohne Feature Selection', 'Nach Feature Selection'),bbox_to_anchor=(1.7, 0.8), loc=5 )   

    
    #autolabel(rects1)
    #autolabel(rects2)
    
    plt.show()

    if feat_access[i] and i!=9 and i!=4 and i!=10:
        fe = 0
        ge = rvFS[i][feat_access_name[i]]
        for j in range(5):
            fe += (ge[j][0][1][-1] + ge[j][1][1][-1] + ge[j][2][1][-1])
        num_features.append(fe/15.)
    elif i!=9 and i!=4 and i !=10:
        num_features.append(rvFS[i][feat_access_name[i]])
    else:
        num_features.append(-1)
    print ('num features: ' +str(num_features[-1]))

print(rvNoFS['setname'])
print('F-SCORES: (SVM, Bayes, DT)')
noFS = rvNoFS['noFSResults'][:,5]  
print( 'noFS:     ' + str(noFS) + '    ROC: ' +str(rvNoFS['noFSResultsRoc']/5.))

for i in range(len(rvFS)):
    fs = rvFS[i][resultsNames[i]][:,5]
    fsRoc = rvFS[i][resultsNames[i]+ 'Roc']
    print(names[i] + '     ' + str(fs) + '    ' + 'Roc' + str(fsRoc[0]) + ' & ' + str(fsRoc[1]) + '    ' + str(num_features[i]))

#rv['dTResultsRoc']
#rv['noFSResultsRoc']



ind = np.arange(3)  # the x locations for the groups
width = 0.3       # the width of the bars

fsSV = rvScoringV['scoringVResults'][:,5]
fsLR = rvLR['lRResults'][:,5]
fsRFE = rvRFE['rFEResults'][:,5]

fig = plt.figure()
ax = fig.add_subplot(111)
rects1 = ax.bar(ind, fsSV, width, color='y')


rects3 = ax.bar(ind+width, fsLR, width, color='r')

rects2 = ax.bar(ind+2*width, fsRFE, width, color='b')

# add some
ax.set_ylabel('F-Score')
ax.set_title('F-Score von Scoring mit Varianz(Scoring V), Recursive Feature Elimination(RFE) und Logistic Regression(LR)\n')
ax.set_xticks(ind+width)
ax.set_xticklabels( ('SVM', 'Bayes', 'DT') )

ax.legend( (rects1[0], rects2[0], rects3[0]), ('Scoring V', 'RFE', 'LR'),loc=5 )


#autolabel(rects1)
#autolabel(rects2)

plt.show()

 
