# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 10:05:31 2016

@author: Catherine
"""
import numpy as np
import readPatterns
import pickle
import scipy
import time
from sklearn.metrics import roc_auc_score
from sklearn.naive_bayes import MultinomialNB
import performance
from sklearn.svm import SVC
import scipy.sparse as sp
from sklearn.tree import DecisionTreeClassifier
import classifierTuning

def classify(testdata, testlabels, traindata, trainlabels, clParas):
    
    times = np.array([0,0,0,0,0,0],dtype=np.float_) # times for fitting and classification
    
    #SVM klassifiziert
    starttime = time.time()
    clf = SVC(C=clParas['optSVMC'], kernel = clParas['optSVMkernel'], probability=True)
    clf.fit(traindata,trainlabels)
    scoresSVM = clf.decision_function(testdata)
    rocSVM = roc_auc_score(testlabels, scoresSVM) 
    times[0] = time.time() - starttime
    starttime = time.time()
    svmResults = clf.predict(testdata)
    times[1] = time.time() - starttime
    perfSVM = performance.performanceEvaluation(svmResults,testlabels)
    
    #Naive Bayes klassifiziert
    starttime = time.time()
    clf = MultinomialNB()
    clf.fit(traindata,trainlabels)
    scoresBayes = clf.predict_proba(testdata)[:,0]
    rocBayes = roc_auc_score(testlabels, scoresBayes)
    MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
    times[2] = time.time()-starttime
    starttime = time.time()
    naiveBayesResults = clf.predict(testdata)
    times[3] = time.time() - starttime

    perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels) 
    
    #Decision Tree klassifiziert
    starttime = time.time()
    clf = DecisionTreeClassifier(max_features=clParas['optDTMaxFeatures'],max_depth=clParas['optDTMaxDepth']) # max_feature nötig?
    clf.fit(traindata,trainlabels) 
    times[4] = time.time() - starttime
    starttime = time.time()
    decisionTreeResults = clf.predict(testdata)
    times[5] = time.time() - starttime
    perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
        
    print("time for fitting the SVM : " + str(times[0]) + "\ntime for SVM classification : " + str(times[1]))
    print("time for fitting Bayes: " + str(times[2]) + "\ntime for Bayes classification : " + str(times[3]))    
    print("time for fitting the decision tree: " + str(times[4]) + "\ntime for Decision Tree classification : " + str(times[5]) + "\n")    
    
    performances = {'perfSVM':perfSVM, 'rocSVM':rocSVM,
                    'perfBayes':perfBayes, 'rocBayes':rocBayes,
                    'perfDT':perfDT,  
                    'times':np.array([times[0],times[1],times[2],times[3],times[4],times[5]])
                    }    
                    
    return performances
    
""" Funktion zum Laden von sparse, csr Matrizen """ 
def load_sparse_csr(filename):
    loader = np.load(filename)
    return sp.csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])    

oneOut = False
oO = "withOnes"
if oneOut:
    oO = "oneOut"

path = "ba/"
databankname = "AIDS99_connected.k10m.t4000.features.binarize"
databankname2 = "AIDS99_connected.k10m.t4000.patterns"
folder = 'Experiment4Maren' #CHANGE

actualTrainrun = 1  # CHANGE Range 1-5

# Select which tree sizes should be considered here:
intervals = []#[2,4],[3,5],[4,6],[5,7],[6,8],[7,9]]

data = load_sparse_csr("baNew/" + databankname + ".sparse." + oO + ".npz")
#data = data.tocsc()   
   
labels = scipy.io.mmread("baNew/"+ databankname + ".label." + oO)  # die zugehörigen Graph-Label
labels = labels.tocsc()

# open dictionary
#d = {0: [12,14,15,17,55], 1:[36,47], 3:[46,68,44], 5:[8,3,66]}
d1 = readPatterns.readPatterns(path, databankname2)
d = readPatterns.savePatterns(d1)
#print(d)

for key in d:
    print(key + " : " + len(d[key]))

print('read patterns')

   

for c in range(len(intervals)):
    treeheightMin = intervals[c][0]
    treeheightMax = intervals[c][1]
    rv = { 'avPerf':np.zeros((3,6)),'avRoc':np.zeros((2,2)), 
    'avTimes':np.zeros((1,6))  }


    for i in range(5): #for i in range(5): #CHANGE
        testindis = np.load('sets/'+ folder + '/testindis'+str(i+1)+'.'+databankname+'.'+oO+'.npy')
        trainindis = np.load('sets/'+ folder + '/trainindis'+str(i+1)+'.'+databankname+'.'+oO+'.npy')
            
        testdata = data[testindis]   
        testlabels = labels[testindis]   
        traindata = data[trainindis]   
        trainlabels = labels[trainindis]   
            
        testdata = testdata.tocsc()  
        traindata = traindata.tocsc()
        trainlabels = trainlabels.todense() 
        trainlabels = np.ravel(trainlabels)
        testlabels = testlabels.todense() 
        testlabels = np.ravel(testlabels) 


        
        
        features = []
        
        for j in range(treeheightMin,treeheightMax+1):
            f = d.get(str(j),[])
            features.extend(f)
        #print(features)
        
        
        selectedTraindata = traindata[:,features]
        selectedTestdata = testdata[:,features]
        
        clParas = {'SVMClinear':[2], 'SVMCrbf':[10], 'SVMgamma':[0.1],
               'dTMaxFeatures':[None], 'dTMaxDepth':[50,70]}
        
        #Lasse alle Algos ohne FS klassifizieren
        optClParas = classifierTuning.tuning(5, selectedTraindata, trainlabels, clParas)
        #noFSResultsDict = classify(testdata, testlabels, traindata, trainlabels, optClParas)
        print("Tuning beendet")
        #noFSResults = np.array((noFSResultsDict['perfSVM'], noFSResultsDict['perfBayes'], noFSResultsDict['perfDT']))     
        #noFSResultsRoc = np.array((noFSResultsDict['rocSVM'], noFSResultsDict['rocBayes']))
        #classifyNoFStime = noFSResultsDict['times']     
        #print("Ohne FS erzielen die Algorithmen eine Performance von: \n \n SVM: \n"+ str(noFSResultsDict['perfSVM'])+ "\n\n Bayes:\n "+ str(noFSResultsDict['perfBayes'])+ "\n \n Decision Tree: \n"+ str(noFSResultsDict['perfDT']))       
        #print("\ntimes of Classification before featureselection: " + str(classifyNoFStime)+ "\nNach der Feature Selection:")
        
        #TODO Kreuzvalidierung
        perf = classify(selectedTestdata, testlabels, selectedTraindata, trainlabels, optClParas)
        
        rv['avPerf'][0] += perf['perfSVM']/5.
        rv['avPerf'][1] += perf['perfBayes']/5.
        rv['avPerf'][2] += perf['perfDT']/5.
        rv['avRoc'][0] += perf['rocSVM']/5.
        rv['avRoc'][1] += perf['rocBayes']/5.
        rv['avTimes'] += perf['times']/5.
        print("Performance in Trainrun "+str(i)+": "+str(perf))
        
        
        
    #dataname ="Experiment1/17Jun151443.AIDS99.oneOut."
    path = "sets/"+ folder
    
    logfilename = databankname +"PS"+ 'treeheights' + str(treeheightMin) + '-' + str(treeheightMax)   # CHANGE
    comment = "Patternsize"                           # Any Comment to this Experiment? 
    
    pickle.dump(perf, open(path + logfilename , "wb") )
    
