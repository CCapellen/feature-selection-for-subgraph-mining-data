# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 12:31:53 2016

@author:  Maren, Catherine

Berechnet Performanz Maße für die Klassifikation 
"""
import numpy as np

  
def performanceEvaluation(algoRes,testlabels): 
    TP = 0 #True Positives
    TN = 0 #True Negatives
    FN = 0 #False Negatives
    FP = 0 #False Positives
    

    numPosTe = 0
    numLigsInTestSet = testlabels.shape[0]
    for i in range(numLigsInTestSet):
        if testlabels[i]==1:
            numPosTe += 1

    for i in range(numLigsInTestSet):
        if testlabels[i]==1 and algoRes[i]==1:
            TP += 1
        else:
            if testlabels[i]==0 and algoRes[i]==1:
                FP += 1
    FN = numPosTe - TP
    TN = numLigsInTestSet-(TP+FP+FN)
    

    
    accuracy = (TP + TN)/ float(numLigsInTestSet) #Overall, how often is the classifier correct?
    errorRate = 1 - accuracy # Overall, how often is it wrong? 
    
    if(numPosTe==0):
        truePositiveRate = 0
    else:    
        truePositiveRate = TP/float(numPosTe) # When it's actually yes, how often does it predict yes? 
        
    if (TP+FP) == 0:
        precision = 0
    else:
        precision = TP / float(TP+FP) # When it predicts yes, how often is it correct? 
    
    if (TN+FP) == 0:
        specifity = 0
    else:
        specifity = TN / float(TN+FP)  # When it's actually no, how often does it predict no?
    
    if(truePositiveRate + precision == 0):
        fValue = 0
    else:       
        fValue = (2*truePositiveRate*precision)/float(truePositiveRate + precision)
    
    
    return np.array([accuracy,errorRate,truePositiveRate,precision,specifity, fValue])
    
    
    