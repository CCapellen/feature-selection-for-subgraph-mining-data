# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:31:58 2016

@author: Maren
"""

import numpy as np
import scipy.sparse as sp
import scipy

#path = "ba/"
#databankname = "AIDS99_connected.k1.t800patterns"

def readPatterns(path, databankname):
    f = open(path + databankname, "r")
    
    dic = {}
    
    for line in f:
        buff = line.split() 
        
        #für jede Zeile
        featureLength = 1
        for e in range(len(buff)):
            #1 am Anfang jeder Zeile ignorieren
            if(e==0):
                continue
            #Speichere Feature ID
            if(e==1):
                featureID = (buff[e])
            #Speichere Größe des Featuregraphen
            else:
                for c in buff[e]:
                    if c == '(':
                        featureLength += 1
            
        
        #Speichere Werte in Dictionary
        w = {str(featureID):str(featureLength)}
        dic.update(w)
        
    return dic

#Funktion bekommt Dictionary mit Featurelängen
#Dic wird "invertiert"
def savePatterns(dic):
    dic2 = {}
    
    #Speichere maximale Featurelänge
    maxLength = 0
    for featureID in dic:
        if int(dic[featureID])>maxLength:
            maxLength = int(dic[featureID])
    
    #Dic 2 mit leeren Listen initialisieren (für jede Länge)
    for l in range(maxLength+1):
        dic2[str(l)]=[]
    
    #FeatureIDs werden nach Länge sortiert in dic2 eingetragen
    for featureID in dic:
        dic2[dic[featureID]].append(featureID)
            
    return dic2
    
#dic = readPatterns(path, databankname)
#dic = savePatterns(dic)
    
    
    