# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 10:26:29 2016

@author: Catherine

Script zur Auwertung der Probabilistiv Frequent Subtree Mining Ergebnisse
"""
import numpy as np
import matplotlib.pyplot as plt
import pickle


foldername = 'Experiment2Cat/Ergebnisse2Cat/'
dataname = 'AIDS99_connected.k1.t800features.binarize'
noFSpickle = 'noFSLINEARrv.p'
apickle = 'logisticRegressionscoringVLINEARrv.p'
bpickle = 'logisticRegressionscoringVLINEARrv.p'


rvNoFS = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
rvB = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )

foldername = 'Experiment3Cat/Ergebnisse3Cat/'
dataname = 'AIDS99_connected.k10m.t4000.features.binarize'
noFSpickle = 'scoringVnoFSLINEARrv.p'
apickle = 'logisticRegressionLINEARrv.p'
bpickle='scoringVnoFSLINEARrv.p'



rvNoFS2 = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA2 = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
rvB2 = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )

foldername = 'Experiment4Cat/Ergebnisse4Cat/'
dataname = 'AIDS99_connected.k10m.t4000absImp5.features.binarize'
noFSpickle = 'noFSLINEARrv.p'
apickle = 'logisticRegressionLINEARrv.p'
bpickle = 'scoringVLINEARrv.p'


rvNoFS3 = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA3 = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
rvB3 = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )


foldername = 'Experiment2Maren/Ergebnisse2Maren/'
dataname = 'AIDS99_connected.k1.t800features.binarize'
noFSpickle = 'noFSLINEARrv.p'
apickle = 'logisticRegressionLINEARrv.p'
#bpickle = 'scoringVLINEARrv.p'


rvNoFS4 = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA4 = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
#rvB4 = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )


foldername = 'Experiment3Maren/Ergebnisse3Maren/'
dataname = 'AIDS99_connected.k10m.t4000.features.binarize'
noFSpickle = 'noFSLINEARrv.p'
apickle = 'logisticRegressionLINEARrv.p'
bpickle = 'scoringVLINEARrv.p'

rvNoFS5 = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA5 = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
rvB5 = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )

foldername = 'Experiment4Maren/Ergebnisse4Maren/'
dataname = 'AIDS99_connected.k10m.t4000absImp5.features.binarize'
noFSpickle = 'noFSLINEARrv.p'
apickle = 'logisticRegressionLINEARrv.p'
bpickle = 'scoringVLINEARSCORING01rv.p'


rvNoFS6 = pickle.load( open( "sets/" + foldername + dataname + noFSpickle, "r" ) )
rvA6 = pickle.load( open( "sets/" + foldername + dataname + apickle, "r" ) )
rvB6 = pickle.load( open( "sets/" + foldername + dataname + bpickle, "r" ) )


for r in [rvB,rvB2,rvB3,rvB5,rvB6]:
    print (r['setname'])
    #print (r['scoringVResults'])
    print (r['nFeatScoringV'])
    print (r['sVFStime'])
    
for r in [rvNoFS, rvNoFS2,  rvNoFS3,  rvNoFS4,  rvNoFS5,  rvNoFS6]:
    print (r['setname'])
    print (r['classifyNoFStime'])
