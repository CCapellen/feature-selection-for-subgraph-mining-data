# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 09:41:33 2016

@author: Maren

Liest Datei "AIDS99..." und speichert Daten als sparse Matrix.
"""

import numpy as np
import scipy.sparse as sp
import scipy

path = "ba/"
writePath = "baNew/"
databankname = "AIDS99_connected.k10m.t4000.features.binarize"
# "AIDS99_connected.k1.t800features.binarize"
#Funktion zum Speichern von sparse Matrizen
def save_sparse(filename,array):
    np.savez(filename,data = array.data ,indices=array.indices,
             indptr =array.indptr, shape=array.shape )            
             
def load_sparse_csr(filename):
    loader = np.load(filename)
    return sp.csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])             

def preprocessData(path, databankname, oneOut):
    f = open(path + databankname, "r")
    
    listPosInd = []    #In dieser Liste werden die Indizes der pos. Beispiele geschrieben, die später in csc-Matrix geschrieben werden
    
    #Listen mit Einträgen die für das Erstellen der sparse Matrix gebraucht werden
    indices = []
    indptr = []
    indptr.append(0)    
    
    
    lineCounter = 0
    newIndi = 0 #speichert höchsten Feature-Indize
    for line in f:
        #lineOut = 1 : Zeile wird nicht berücksichtigt (oneOut)
        lineOut = 0
        buff = line.split()
        if(len(buff)<2):     #Wenn Eintrag keine Features hat, wird er übersprungen       
                continue
            
        for e in range(len(buff)):
            if(e==0):       
                if(buff[e]=="2"): # 1. Eintrag: Beispiel pos. oder neg.?
                    listPosInd.append(lineCounter) # Indizes des pos. Beispiele, beg. bei 0
                #Wenn die Option "oneOut" aktiviert ist, werden Beispiele mit Klassifikation "1" ignoriert, ansonsten als neg. Beispiele klassifiziert
                if(buff[e]=="1" and oneOut == 1):
                    lineOut = 1
                    break
            
            else:  
                #nach Doppelpunkt abschneiden
                buffNew = buff[e].split(":")
                indices.append(int(buffNew[0]))
                if(int(buffNew[0])>int(newIndi)):
                    newIndi = int(buffNew[0])        #Falls Indize größer als bisheriges Maximum, ersetze dieses
        
        if(lineOut != 1):
            indptr.append(len(indices)) 
            lineCounter += 1    
    
    f.flush()
    f.close()
    
    data = np.ones((len(indices)))

    d = sp.csr_matrix((data, indices, indptr), (lineCounter, newIndi))  
    #1. Spalte abschneiden, da sie nur Nullen enthält
    d = d[:,1:]
    print(lineCounter)
    if(oneOut==1):
        save_sparse(writePath + databankname + ".sparse.oneOut",d)
        #scipy.io.mmwrite(writePath + databankname + ".sparse.oneOut",d)
    else:
        save_sparse(writePath + databankname + ".sparse.withOnes",d)
        #scipy.io.mmwrite(writePath + databankname + ".sparse.withOnes",d)
    
    #listPosInd wird als csc-Matrix gespeichert
    arrPosInd = np.zeros((lineCounter))
    j = 0
    for i in range(lineCounter):
        if(j>=len(listPosInd)):
            break
        if(listPosInd[j]==i):
            arrPosInd[i] = 1
            j += 1
    
    arrPosInd = arrPosInd.reshape(-1,1) 
    spArrPosInd = sp.csc_matrix(arrPosInd)
    if(oneOut == 1):
        #save_sparse(writePath + databankname + ".label.oneOut",spArrPosInd)
        scipy.io.mmwrite(writePath + databankname + ".label.oneOut",spArrPosInd)
    else:
        #save_sparse(writePath + databankname + ".label.withOnes",spArrPosInd)
        scipy.io.mmwrite(writePath + databankname + ".label.withOnes",spArrPosInd)
preprocessData(path, databankname, 1)
preprocessData(path, databankname, 0)
  
#a = load_sparse_csr(writePath + databankname + ".classification.sparse.npz")
#print(a.indices)
#print(listPosInd)
  