# -*- coding: utf-8 -*-
"""
Created on Sat Jul 09 18:09:47 2016

@author: Catherine 

Code des genetischen Algorithmus:
Die Funktion classi führt die Klassifikation für die 
Berechnung der Fitnesswertes durch.

"""

import numpy as np 
import random
from sklearn.svm import SVC
import performance
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import KFold




def classi(testdata, testlabels, traindata, trainlabels, classificator):
    if classificator =='DT':
        #Decision Tree klassifiziert
        clf = DecisionTreeClassifier() # max_feature nötig?
        clf.fit(traindata,trainlabels) 
        decisionTreeResults = clf.predict(testdata)
        perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
        return perfDT
            
    #Naive Bayes klassifiziert
    if classificator == 'Bayes':
        clf = MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
        clf.fit(traindata,trainlabels)
        
        naiveBayesResults = clf.predict(testdata)
        perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels)  
        return perfBayes 
    
    if classificator == 'SVM':
        clf = SVC(C=2, kernel = 'linear')
        clf.fit(traindata,trainlabels)
        svmResults = clf.predict(testdata)
        perfSVM = performance.performanceEvaluation(svmResults,testlabels) #sp.csr_matrix(
        return perfSVM 
        
    if classificator == 'BD':
        clf = DecisionTreeClassifier() # max_feature nötig?
        clf.fit(traindata,trainlabels) 
        decisionTreeResults = clf.predict(testdata)
        perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
        clf = MultinomialNB()
        clf.fit(traindata,trainlabels)
        MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
        naiveBayesResults = clf.predict(testdata)
        perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels)
        return perfDT + perfBayes
        
    if classificator == 'SBD':
        clf = DecisionTreeClassifier() # max_feature nötig?
        clf.fit(traindata,trainlabels) 
        decisionTreeResults = clf.predict(testdata)
        perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels)
        clf = MultinomialNB()
        clf.fit(traindata,trainlabels)
        MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)
        naiveBayesResults = clf.predict(testdata)
        perfBayes = performance.performanceEvaluation(naiveBayesResults, testlabels)  
        clf = SVC(C=2, kernel = 'linear')
        clf.fit(traindata,trainlabels)
        svmResults = clf.predict(testdata)
        perfSVM = performance.performanceEvaluation(svmResults,testlabels) #sp.csr_matrix(
        return perfSVM + perfBayes+ perfDT
    
    if classificator == 'SD':
        clf = DecisionTreeClassifier() # max_feature nötig?
        clf.fit(traindata,trainlabels) 
        decisionTreeResults = clf.predict(testdata)
        perfDT = performance.performanceEvaluation(decisionTreeResults, testlabels) 
        clf = SVC(C=2, kernel = 'linear')
        clf.fit(traindata,trainlabels)
        svmResults = clf.predict(testdata)
        perfSVM = performance.performanceEvaluation(svmResults,testlabels) #sp.csr_matrix(
        return perfSVM + perfDT
                     
        

def ga(data,labels,iterations,numGenes,mut_prob,classificator,restart_value):
"""
restart_value bezeichnet die Anzahl an Iterationen in denen sich die Performanz des besten Features nicht verändert, bevor eine Neuinitialisierung durchgeführt wird
"""    
    it = iterations
    restarter = restart_value
    genes = []
    num_samples,num_feat = data.shape
    best_perf_last = 0  
    numCV = 5
    cv = KFold(num_samples, n_folds=numCV, shuffle=True)
    
    # intialization
    for j in range(numGenes):
        temp = np.random.choice([0,1],num_feat)
        genes.append(np.array(temp,dtype=np.bool))
        
    # selection
    for i in range(it):    
        best_gene = -1
        best_perf = -1
        gene_perf = {}
        for g in range(len(genes)):
            perf = 0
            for train,test in cv:
                traindata = data[train]
                trainlabels = labels[train]
                testdata = data[test]
                testlabels = labels[test]
                perf += (1./numCV)*classi(testdata[:,genes[g]],testlabels, traindata[:,genes[g]], trainlabels,classificator)[5]         
            gene_perf[g] = np.int(1000*perf) #scale to use as weights
            if perf > best_perf:  # elitism
                best_perf = perf
                best_gene = genes[g]
        print('best_perf=' + str(best_perf))
        if best_perf_last == best_perf:
            restarter -= 1
        else:
            restarter = restart_value
        best_perf_last = best_perf
        if restarter == 0:
            print('restart!')
            #restart but keep best
            for j in range(numGenes-1): 
                temp = np.random.choice([0,1],num_feat)
                genes.append(np.array(temp,dtype=np.bool))
            genes.append(best_gene)
            restarter = restart_value
        
        #Recombination   
        newGenes = []
        for j in range(int((numGenes-1)/2)):
            a = random.choice([x for x in gene_perf for y in range(gene_perf[x])])
            b = a
            while b==a:
                b = random.choice([x for x in gene_perf for y in range(gene_perf[x])])
            rand = random.randint(0,num_feat)
            c = np.concatenate((genes[a][:rand],genes[b][rand:]))
            d = np.concatenate((genes[b][:rand],genes[a][rand:]))
            newGenes.append(c)
            newGenes.append(d)
        #Mutation
        for j in range(len(genes)):
            mut_prob= int(mut_prob)*1000
            prob = {1:mut_prob , 0:1-mut_prob}
            for k in range(num_feat):
                if random.choice([x for x in prob for y in range(prob[x])]):
                    if(newGenes[j][k]):
                        newGenes[j][k] = 0
                    else:
                        newGenes[j][k] = 1
        newGenes.append(best_gene)
        genes = newGenes
        
    for g in range(len(genes)):
        perf = 0
        for train,test in cv:
            traindata = data[train]
            trainlabels = labels[train]
            testdata = data[test]
            testlabels = labels[test]
            perf += (1./numCV)*classi(testdata[:,genes[g]],testlabels, traindata[:,genes[g]], trainlabels,classificator)[5]           
        if perf > best_perf:  # elitism
            best_perf = perf
            best_gene = genes[g]
    return best_gene
    