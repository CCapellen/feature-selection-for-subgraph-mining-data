# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 14:46:52 2016

@author: Maren

Ermittelt die optimalen Parameter für die Klassifikatoren. 
Wird verwendet für die Klassifikation ohne Feature Selection, 
sowie für die Klassifikation nach der Feature Selection durch
einen Algorithmus, der keine Parameter verwendet.
"""
import numpy as np
import scipy.sparse as sp
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import KFold
import performance

def tuning(tuneRuns, data, labels, clParas):
    #Parametertupel werden erstellt    
    SVMCLinear = clParas['SVMClinear']
    SVMParaCombisRbf = [(x,y) for x in clParas['SVMCrbf'] for y in clParas['SVMgamma']]
    dTParaCombis = [(x,y) for x in clParas['dTMaxFeatures'] for y in clParas['dTMaxDepth']]
    
    #Trainings- und Testmenge für das Tuning werden erstellt
    n_samples, n_features = data.shape    
    numCV = tuneRuns
    cv = KFold(n_samples, n_folds=numCV, shuffle=True)
    
    #return values Dictionary wird erstellt
    rvTuning = {'optSVMC':0, 'optSVMkernel':'linear', 'optSVMgamma':0, 'optDTMaxFeatures':0, 'optDTMaxDepth':0}
       
    SVMResultsLinear = np.zeros(len(clParas['SVMClinear'])) 
    SVMResultsRbf = np.zeros(len(clParas['SVMCrbf'])*len(clParas['SVMgamma'])) 
    dTResults = np.zeros(len(clParas['dTMaxFeatures'])*len(clParas['dTMaxDepth']))
    
    for train, test in cv:
        #Trainings- und Testsets für jew. Durchlauf
        traindata = data[train]
        trainlabels = labels[train]
        testdata = data[test]
        testlabels = labels[test]        
        
        #Passendes Format
        testdata = testdata.tocsc()  
        traindata = traindata.tocsc()
        #testlabels = sp.csc_matrix(testlabels)
        
        #SVM wird optimiert für linearen Kernel
        j = 0
        for SVMC in SVMCLinear:              
            clf = SVC(C=SVMC, kernel = 'linear')
            clf.fit(traindata,trainlabels)
            svmResults = clf.predict(testdata)
                
            perfSVM = performance.performanceEvaluation(svmResults,testlabels)
                
            perfSVM /= tuneRuns
                
            SVMResultsLinear[j] += perfSVM[5]
            j += 1   
        
        
        #SVM wird optimiert für Rbf-Kernel
        j=0
        for paraCombi in SVMParaCombisRbf:
            clf = SVC(C=paraCombi[0], kernel = 'rbf', gamma=paraCombi[1])
            clf.fit(traindata,trainlabels)
            svmResults = clf.predict(testdata)
            
            perfSVM = performance.performanceEvaluation(svmResults,testlabels)
                
            perfSVM /= tuneRuns
                
            SVMResultsRbf[j] += perfSVM[5]
            j += 1  
            
        #Decision Tree wird optimiert        
        j = 0
        for paraCombi in dTParaCombis:                
            clf = DecisionTreeClassifier(max_features=paraCombi[0],max_depth=paraCombi[1]) # max_feature nötig?
            clf.fit(traindata,trainlabels) 
            decisionTreeResults = clf.predict(testdata)
            perfDT = performance.performanceEvaluation(decisionTreeResults,testlabels)
                
            perfDT /= tuneRuns
                
            dTResults[j] += perfDT[5]
            j += 1         
    
    #optimale Paracombi für SVM mit linearem Kernel
    pMax = 0
    i = 0
    pIndi = 0
    for p in SVMResultsLinear:
        if p > pMax:
            pMax = p
            pIndi = i
        i += 1    
        
    optSVMCLinear = SVMCLinear[pIndi] 
    optResultsLinear = SVMResultsLinear[pIndi]

    #optimale Paracombi für SVM mit lRbf-Kernel
    pMax = 0
    i = 0
    pIndi = 0
    for p in SVMResultsRbf:
        if p > pMax:
            pMax = p
            pIndi = i
        i += 1    
        
    optParaCombiSVM = SVMParaCombisRbf[pIndi] 
    optResultsRbf = SVMResultsRbf[pIndi]
    
    if(optResultsLinear>optResultsRbf):
        print("SVM Paracombi mit C=" + str( optSVMCLinear) + " sowie linearem Kernel erzielt optimale Ergebnisse:" + str(optResultsLinear))
        rvTuning['optSVMC'] = optSVMCLinear
        rvTuning['optSVMkernel'] = 'linear'
    else:    
        print("SVM Paracombi mit C=" + str( optParaCombiSVM[0]) + " und Gamma=" + str(optParaCombiSVM[1]) + " sowie Rbf-Kernel erzielt optimale Ergebnisse:" + str(optResultsRbf)) 
        rvTuning['optSVMC'] = optParaCombiSVM[0]
        rvTuning['optSVMkernel'] = 'rbf'
        rvTuning['optSVMgamma'] = optParaCombiSVM[1]
    
    
    
    #Optimale Paracombi für DT
    pMax = 0
    i = 0
    pIndi = 0
    for p in dTResults:
        if p > pMax:
            pMax = p
            pIndi = i
        i += 1    
        
    optParaCombiDT = dTParaCombis[pIndi] 
    print("Decision Tree Paracombi mit maxFeatures=" + str( optParaCombiDT[0]) + " und maxDepth=" + str(optParaCombiDT[1]) + " erzielt optimale Ergebnisse:" + str(dTResults[pIndi])) 
    rvTuning['optDTMaxFeatures'] = optParaCombiDT[0]
    rvTuning['optDTMaxDepth'] = optParaCombiDT[1]
    #bla
    return rvTuning
    