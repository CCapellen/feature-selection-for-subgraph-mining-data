# -*- coding: utf-8 -*-
"""
Created on Wed May 18 10:11:05 2016

@author: Maren, Cat

Adaption von experiment.py, die nur eine Kreuzvalidierung ausführt. Dies kann verwendet werden um die Berechnung von Experimenten mit 
langer Laufzeit aufzuteilen. Die Variable actualTrainrun muss hierfür auf die Werte 1 bis 5 gesetzt werden.
"""
import training2
import datetime
import pickle
import scipy
import numpy as np

algos = {'noFS':0,
         'scoringV':1,
         'scoringC':0,
         'scoringF':0,
         'decisionTreeFilter':0,
         'relief':0,
         'logisticRegression':0,
         'randomForestFeatureSelection':0,
         'recursiveFeatureElimination':0,
         'svmRandomForwardWrapper':0,
         'decisionStumps':0,
         'mrmr':0
         }
         
fsParas = {'scoringCThresholds':[1000],
           'scoringFThresholds':[1000],  
           'rFNumTrees':[100,200], 'rFD':[150,200,250],
           'dTmaxDepth':[None,50,60,100], 'dTThresholds':[135,150,175,200,],
           'rFEStep':[0.01,0.1], 'rFENFeaturesToSelect':[10000,5000,3000],
           'dSD':[5000,10000,15000],
           'mrmrNFeat':2}

clParas = {'SVMClinear':[1], 'SVMCrbf':[2], 'SVMgamma':[0.1],
           'dTMaxFeatures':[None], 'dTMaxDepth':[50,70]} #dtMaxFeatures muss kleiner als kleinster Threshold sein

foldername = "Experiment1Maren/"         
dataname = "AIDS99.cpk" # 'AIDS99_connected.k1.t800features.binarize'
oneOut = False
actualTrainrun = 1


rv = training2.training(algos, fsParas, clParas, dataname, foldername, actualTrainrun, oneOut)
rv['fsParas'] = fsParas
rv['clParas'] = clParas
rv['algos'] = algos


# Bestimme Algorithmennamen für den Logfilenamen
algonames = ''
for key in algos.keys():
    if algos[key] != 0:
        algonames += key



logfilename = "sets/"+ foldername + dataname + algonames + '.Trainrun' + str(actualTrainrun)   # CHANGE 
comment = "trainruntest" # Kommentare zu diesem Experiment?
rv['comment'] = comment

pickle.dump( rv, open( logfilename + "rv.p", "wb" ) )

# Erstelle logfile
time = str(datetime.datetime.utcnow())
log = open(logfilename,"w")
log.write(comment + "\n")
log.write(time  + "\n \n")

log.write(str(algos))
log.write(str(rv))

log.flush()
log.close()
