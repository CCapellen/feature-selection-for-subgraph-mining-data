# -*- coding: utf-8 -*-
"""
Created on Tue May  3 09:36:42 2016

@author: Maren, Catherine

In diesem Modul sind fast alle verwendeten FS-Algorithmen eingebunden.
Die Methoden werden von den anderen Modulen (Training, Tuning) aufgerufen.
"""

import numpy as np
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import RFE
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import f_classif
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from skfeature.function.wrapper import svm_forward_random
import reliefSelfNew
from skfeature.function.information_theoretical_based import MRMR
import ga2


def scoringVariance(traindata, trainlabels):
    selector = VarianceThreshold(threshold=0.1)
    selector.fit(traindata, trainlabels)
    indices = selector.get_support(indices=True)
    return indices  
    
def scoringChi2(traindata, trainlabels, d):
    selector = SelectKBest(chi2, k=d)
    selector.fit(traindata, trainlabels)
    indices = selector.get_support(indices=True)
    return indices
    
def scoringFValue(traindata, trainlabels, d):
    selector = SelectKBest(f_classif, k=d)
    selector.fit(traindata, trainlabels)
    indices = selector.get_support(indices=True)
    return indices   
    
def randomForestFS(traindata,trainlabels, numTrees,d):
    forest = ExtraTreesClassifier(n_estimators=numTrees) #, max_features=maxFeaturesConsidered
    forest.fit(traindata,trainlabels)
    importances = forest.feature_importances_
    indices = np.argsort(importances)[::-1]
    return indices[:d]

    
def relief(traindata, trainlabels, m, d):
    indices = reliefSelfNew.relief(traindata, trainlabels, m)
    return indices[:d]   

def mrmr(traindata,trainlabels,nFeat): 
    return MRMR.mrmr(traindata, trainlabels, n_selected_features=nFeat)

def logisticRegression(traindata, trainlabels):
    lr = LogisticRegression(penalty="l1", dual=False)
    lr.fit(traindata, trainlabels)
    model = SelectFromModel(lr, prefit=True)
    indices = model.get_support(indices=True)
    return indices

def decisionTreeFilter(traindata, trainlabels, depth, threshold):
    clf = DecisionTreeClassifier(max_depth=depth) 
    clf.fit(traindata,trainlabels)
    importances = clf.feature_importances_

    indices = np.argsort(importances)[::-1]
    return indices[:threshold]

def recursiveFeatureElimination(traindata, trainlabels,step,nFeaturesToSelect):
    estimator = SVR(C=1.0, kernel="linear")
    selector = RFE(estimator, step=step,n_features_to_select=nFeaturesToSelect).fit(traindata, trainlabels)
    indices = selector.get_support(indices=True)
    return indices

def decisionStumps(traindata, trainlabels, d):
    clf = ExtraTreesClassifier(max_features=None, max_depth=1)
    ada = AdaBoostClassifier(base_estimator = clf).fit(traindata,trainlabels)
    importances = ada.feature_importances_

    indices = np.argsort(importances)[::-1]
    return indices[:d]
    
def svmRandomForwardWrapper(traindata, trainlabels, nFeaturesToSelect, nFeaturesConsidered):
    return svm_forward_random.svm_forward(traindata,trainlabels,nFeaturesToSelect,nFeaturesConsidered)
    
def geneticAlgorithm(data,labels,iterations,numGenes,mut_prob,classificator):
    restart_value = 5
    len_gen = int(0.1 * data.shape[1])
    return ga2.ga(data,labels,iterations,numGenes,len_gen,mut_prob,classificator,restart_value)
    