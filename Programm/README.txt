Die Ordnerstruktur:

ba/ enth�lt die Daten in ihrer urspr�nglichen Form

baNew/ enth�lt die Daten in der pr�prozessierten Form

sets/ enth�lt die zwischengespeicherten Indizes f�r die 
Kreuzvalidierung sowie die logfile Dateien

sklearn/ enh�lt die Module, die aus dem Paket sklearn stammen
(und von uns adaptiert wurden)


Kurze Beschreibung der Module:

experiment: Script, in dem Parameter f�r ein Experiment
eingestellt werden k�nnen.
Das Modul wurde f�r die Durchf�hrung aller Experimente auf
den Cyclic Pattern Kernel Daten verwendet.
Erstellt ein logfile in textform, sowie ein pickle file, 
welche im gleichen Ordner, wie die zwischengespeicherten
Testindizes zu finden sind.
Die pickle Dateien werden von python 2 und python 3 anders 
gespeichert und k�nnen nicht von der jeweils anderen Version 
ge�ffnet werden!
(Ein Konvertierungstool picklec.py von python 3 zu zwei ist 
in diesem Ordner)

experimentLinear: 
F�hrt experiment mit linearer SVM (sowie den anderen 
Klassifikatoren) aus. Ruft daf�r trainingLinearSVM und 
classifierTuningLinearSVM auf

patternSizeFS:
Skript mit dem die Experimente zur Feature Selection nach
Teilbaumgr��e durchgef�hrt wurden.


preprocessData:
Liest Datei ein und speichert Daten als Sparse-Matrix

sets:
Erzeugt Teilmengen (1:5) aus den Ursprungsdaten

training:
Hauptprogramm; ruft Klass.-Algos und feature Selection auf


featureSelection:
Funktionen f�r FS Algorithmen (aus training aufrufbar)

performance:
Liefert Performanzma�e f�r Klass.-Algos

Auswertung ...: 
Auswertungsskripte f�r Experimente