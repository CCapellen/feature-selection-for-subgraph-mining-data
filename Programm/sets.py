# -*- coding: utf-8 -*-
"""
Spyder Editor
 
Liest die Daten im Sparse Format ein und erstellt Trainings- und Testsets.

Parameter:
oneOut = True : beim Binarisieren der Daten wurden die Beispiele mit Aktivitäts-
                label = 1 aussortiert
"""

import numpy as np
import scipy.sparse as sp
import scipy

from sklearn import cross_validation
from sklearn import datasets
from sklearn import svm
from sklearn.cross_validation import train_test_split

import time

""" Funktion zum Laden von sparse, csr Matrizen """ 
def load_sparse_csr(filename):
    loader = np.load(filename)
    return sp.csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])
                         
oneOut = False
dataset = "AIDS99.cpk" #"AIDS99.cpk" #"AIDS99_connected.k1.t800features.binarize" 

oO = "withOnes"
if oneOut:
    oO = "oneOut"
                         
# Lade Daten, die im Preprocessing Schritt erstellt wurden 
#alt
#data = load_sparse_csr("baNew/" + dataset + ".sparse" + oO + ".npz")
#data = data.tocsc()

labels = scipy.io.mmread("baNew/"+ dataset + ".label." + oO)  # die zugehörigen Graph-Label
labels = labels.tocsc()  #konvertiere um array slicing möglich zu machen


indis = np.array(range(labels.shape[0]))
np.random.shuffle(indis)
fifth = np.round(labels.shape[0]/5)

# Erstelle disjunkte Indizes für alle 5 Trainings- und Testmengen
testindis1 = indis[:fifth]
trainindis1 = indis[fifth:]

testindis2 = indis[fifth:(fifth*2)]
trainindis2 = np.concatenate((indis[:fifth],indis[(fifth*2):]))

testindis3 = indis[fifth*2:fifth*3]
trainindis3 = np.concatenate((indis[:2*fifth],indis[(fifth*3):]))

testindis4 =  indis[fifth*3:fifth*4]
trainindis4 = np.concatenate((indis[:3*fifth],indis[(fifth*4):]))

testindis5 =  indis[-fifth:]
trainindis5 = indis[:-fifth]

np.save("sets/testindis1."+dataset+"."+oO, testindis1)
np.save("sets/trainindis1."+dataset+"."+oO, trainindis1)
np.save("sets/testindis2."+dataset+"."+oO, testindis2)
np.save("sets/trainindis2."+dataset+"."+oO, trainindis2)
np.save("sets/testindis3."+dataset+"."+oO, testindis3)
np.save("sets/trainindis3."+dataset+"."+oO, trainindis3)
np.save("sets/testindis4."+dataset+"."+oO, testindis4)
np.save("sets/trainindis4."+dataset+"."+oO, trainindis4)
np.save("sets/testindis5."+dataset+"."+oO, testindis5)
np.save("sets/trainindis5."+dataset+"."+oO, trainindis5)

"""
#alt

# Erstelle Matrizen Set 1
testlabels1 = labels[testindis1]
trainlabels1 = labels[trainindis1]
testdata1 = data[testindis1]
traindata1 = data[trainindis1]

# Erstelle Matrizen Set 2
testlabels2 = labels[testindis2]
trainlabels2 = labels[trainindis2]
testdata2 = data[testindis2]
traindata2 = data[trainindis2]

# Erstelle Matrizen Set 3
testlabels3 = labels[testindis3]
trainlabels3 = labels[trainindis3]
testdata3 = data[testindis3]
traindata3 = data[trainindis3]

# Erstelle Matrizen Set 4
testlabels4 = labels[testindis4]
trainlabels4 = labels[trainindis4]
testdata4 = data[testindis4]
traindata4 = data[trainindis4]

# Erstelle Matrizen Set 5
testlabels5 = labels[testindis5]
trainlabels5 = labels[trainindis5]
testdata5 = data[testindis5]
traindata5 = data[trainindis5]

# Erstelle eindeutigen Namen für Testmatrizen
t = time.ctime() 
tStr = t[8:10] + t[4:7] + t[11:13] + t[14:16] + t[17:19]

# Speichern der Matrizen
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testlabels1',testlabels1)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.trainlabels1',trainlabels1)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testdata1',testdata1)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.traindata1',traindata1)

scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testlabels2',testlabels2)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.trainlabels2',trainlabels2)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testdata2',testdata2)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.traindata2',traindata2)

scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testlabels3',testlabels3)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.trainlabels3',trainlabels3)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testdata3',testdata3)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.traindata3',traindata3)

scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testlabels4',testlabels4)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.trainlabels4',trainlabels4)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testdata4',testdata4)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.traindata4',traindata4)

scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testlabels5',testlabels5)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.trainlabels5',trainlabels5)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.testdata5',testdata5)
scipy.io.mmwrite('sets/' + tStr + "." + dataset + oO + '.traindata5',traindata5)




#B = scipy.io.mmread('trainingsset1') #Funktion zum Lesen
"""