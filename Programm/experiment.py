# -*- coding: utf-8 -*-
"""
Created on Wed May 18 10:11:05 2016

@author: Maren, Cat

Dieses Modul startet einen Testlauf von auswählbaren Feature Selection Algorithmen
mit  Parametertuning und n-facher Kreuzvalidierung.
Der gewünschte Datensatz und die Binarisierungsmethode lassen sich ebenfalls hier
einstellen.
Außerdem werden die Parameter angegeben, die im Parametertuning
optimiert werden sollen. Die anderen Parameter, die eingestellt werden können finden sich in training.py. 
Die Varianz von Scoring mit Varianz kann in featureSelection.py eingestellt werden.
"""
import training
import datetime
import pickle
import scipy
import numpy as np


#Algorithmen, die man auswählen will (=1)
algos = {'noFS':0,
         'scoringV':0,
         'scoringC':0,
         'scoringF':0,
         'decisionTreeFilter':0,
         'relief':0,
         'logisticRegression':0,
         'randomForestFeatureSelection':0,
         'recursiveFeatureElimination':0,
         'svmRandomForwardWrapper':0,
         'decisionStumps':0,
         'mrmr':0,
         'geneticAlgorithm':0,
         'posSelect':0
         }
 
#Parameter, die getestet werden sollen 
#Für die Feature Selection       
fsParas = {'scoringCThresholds':[15000],
           'scoringFThresholds':[5000,10000,15000,20000,25000],  
           'rFNumTrees':[400], 'rFD':[150,200],
           'dTmaxDepth':[None,50,60,100], 'dTThresholds':[135,150,175,200,],
           'rFEStep':[0.3], 'rFENFeaturesToSelect':[10000,5000,2000],
           'dSD':[5000,10000,15000],
           'mrmrNFeat':10}

#Für die Klassifikation       
clParas = {'SVMClinear':[1,2,2.5,3], 'SVMCrbf':[10,13,15], 'SVMgamma':[0.07,0.1,0.15],
           'dTMaxFeatures':[None], 'dTMaxDepth':[50,70,90]} #dtMaxFeatures muss kleiner als kleinster Threshold sein

#weitere Einstellungen (Dateiname)
foldername = "Experiment1Maren/"         
dataname = 'AIDS99.cpk' 
oneOut = False

#Aufruf des Trainings
rv = training.training(algos, fsParas, clParas, dataname, foldername, oneOut)
rv['fsParas'] = fsParas
rv['clParas'] = clParas
rv['algos'] = algos


# Bestimme Algorithmen, die benutzt werden für den Namen des Logfiles
algonames = ''
for key in algos.keys():
    if algos[key] != 0:
        algonames += key


# Erstelle Logfile
logfilename = "sets/"+ foldername + dataname + algonames +'LINEARScoring0001'  # CHANGE
comment = ""
rv['comment'] = comment                           # Any Comment to this Experiment? 

pickle.dump( rv, open( logfilename + "rv.p", "wb" ) )

# Speichere logfile
time = str(datetime.datetime.utcnow())
log = open(logfilename,"w")
log.write(comment + "\n")
log.write(time  + "\n \n")

log.write(str(algos))
log.write(str(rv))

log.flush()
log.close()
