# -*- coding: utf-8 -*-
"""
Created on Tue May 31 09:24:43 2016

@author: Maren

Berechnet das Integral der Roc-Kurve für einen SVM- oder Scoring-Classifier
"""

import numpy as np
import performance
import scipy
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
import matplotlib.pyplot as plt
 
testdata = scipy.io.mmread('sets/testdata' + str((1)))
testlabels = scipy.io.mmread('sets/testlabels' + str((1)))
traindata = scipy.io.mmread('sets/traindata' + str((1)))
trainlabels = scipy.io.mmread('sets/trainlabels' + str((1)))
        
testdata = testdata.tocsc()  
traindata = traindata.tocsc()
trainlabels = trainlabels.todense() 
trainlabels = np.ravel(trainlabels) 
testlabels = testlabels.todense() 
testlabels = np.ravel(testlabels) 
   

"""
Modifizierte classify-Methode (ohne Decision Tree)
"""
def rocCurve(traindata, trainlabels, testdata, testlabels):
    
    #Roc-Curve für SVM
    clf = SVC(C=1.0, kernel = 'linear', probability=True)
    scoresSVM = clf.fit(traindata,trainlabels).decision_function(testdata)   
    rocAreaSVM = roc_auc_score(testlabels, scoresSVM) 
    
    #Roc-Curve für Bayes
    clf = MultinomialNB()
    clf.fit(traindata,trainlabels)
    scoresBayes = clf.predict_proba(testdata)[:,1] #?
    rocAreaBayes = roc_auc_score(testlabels, scoresBayes)
    
    print(rocAreaSVM, rocAreaBayes)
    
    return np.array([rocAreaSVM, rocAreaBayes])

    
    