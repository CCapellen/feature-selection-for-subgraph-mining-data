Hier sollen sich die unpr�prozessierten Daten befinden, damit sie von 
preprocessData.py als sparse Matrizen eingelesen werden k�nnen.

Alle Feature Selection Algorithmen, die in experiment.py ausgew�hlt werden
k�nnen ben�tigen nur die pr�prozessierten Daten.
Um die Feature Selection nach Teilbaumgr��e in patternSizeFS.py durchf�hren 
zu k�nnen, werden die .patterns Dateien ben�tigt.