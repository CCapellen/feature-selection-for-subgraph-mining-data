# -*- coding: utf-8 -*-
"""
Created on Fri Jul 22 13:12:40 2016

@author: Maren, Catherine 

Skript zur Auswertung der Feature Selection nach Baumgröße
"""
import pickle
import matplotlib.pyplot as plt
import numpy as np

foldername = 'treesizeFS4000abs/'
heights = ['1-1','2-2','3-3','4-4','5-5','6-6','7-7','8-8','9-9']
withones = 'Maren'
name = 'Experiment4' + withones + 'AIDS99_connected.k10m.t4000absImp5.features.binarizePStreeheights'
rvs = []
"""
def autolabel(rects,numbars):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/np.float(numbars), 1.05*height, '%d'%int(height),
                ha='center', va='bottom')
"""
for i in heights:
    rvs.append(pickle.load( open( "sets/" + foldername + name +  i, "r" ) ))
"""

width = 0.2       # the width of the bars    

fig = plt.figure()
ax = fig.add_subplot(111)
"""
for rv in rvs:
    print ('SVM:' + str(rv['perfSVM'][5]))
    print( 'Bayes: ' + str(rv['perfBayes'][5]))
    print ('DT:' + str(rv['perfDT'][5]))
    print ('ROC SVM:' + str(rv['rocSVM']))
    print( 'ROC Bayes: ' + str(rv['rocBayes']) + '\n')
"""    

rects1 = ax.bar(0.15, rvs[1]['perfSVM'][5], width, color='g')    
rects2 = ax.bar(0.15 + width, rvs[2]['perfSVM'][5], width, color='b')
rects3 = ax.bar(0.15 + 2*width, rvs[3]['perfSVM'][5], width, color='g')    
rects4 = ax.bar(0.15 + 3*width, rvs[4]['perfSVM'][5], width, color='b')
rects5 = ax.bar(0.15 + 4*width, rvs[5]['perfSVM'][5], width, color='g')    
rects6 = ax.bar(0.15 + 5*width, rvs[6]['perfSVM'][5], width, color='b')
rects7 = ax.bar(0.15 + 6*width, rvs[7]['perfSVM'][5], width, color='g')
rects8 = ax.bar(0.15 + 7*width, rvs[8]['perfSVM'][5], width, color='g')    


# add some
ax.set_ylabel('F-Score')
ax.set_title('WithOnes')
    
#ax.set_xticks(width)
ax.set_xticklabels( ( '1', '2', '3', '4', '5', '6', '7', '8', '9') )

#ax.legend( (rects1[0], rects1[0]), ('ohne Feature Selection', 'Nach Feature Selection'),bbox_to_anchor=(1.7, 0.8), loc=5 )   

    
#autolabel(rects1)
#autolabel(rects2)
    
plt.show()
"""